# Conceção e Implementação de Aplicações para a Internet

## Delivery

### 1 - Commit a34faab

* [Detailed API](documents/API/APIDetailed.pdf)
* [Simple API](documents/API/APISimplified.pdf)
* [ER Diagram](diagrams/ER/P01_ER_Diagram.png)

### 2 - Commit 5045015

* [Report](documents/Reports/Report-Phase1.pdf)
* [Controllers](/Server/src/main/kotlin/fct/ciai/api)
* [Services](/Server/src/main/kotlin/fct/ciai/services)
* [Repositories](/Server/src/main/kotlin/fct/ciai/repositories)
* [Spring Security](/Server/src/main/kotlin/fct/ciai/configurations/security)
* [Authorizations](/Server/src/main/kotlin/fct/ciai/authorization)
* [JSON Web Token](/Server/src/main/kotlin/fct/ciai/configurations/security/JWTUtil.kt)
* [Tests](/Server/src/test/kotlin/fct/ciai)
* [Python Data Base Inserts Script - HTTP Client](scripts/generateHTTP.py)
* [Populate Data Base File](/Server/populateApp.http)

### 3 - Commit 029bdca

* [Report: IFML Diagrams + Mock-up](documents/Reports/Report-Phase3.pdf)
* [IFML Diagrams](diagrams/IFML/IFML-Diagram.pdf)
* [Mock-up](diagrams/Mockup/MockUp.pdf)

### 4 Commit bd9a4ba

* [Final Report](documents/Reports/Final-Report.pdf)

## Setup

### Git Clone
```bash
git clone https://rfalopes@bitbucket.org/rfalopes/ciai-project.git
```

### Install Maven
[Maven install guide](https://www.baeldung.com/install-maven-on-windows-linux-mac)

---

## Run and test with Maven

### Run
```cmd
cd Server
mvn spring-boot:run
```

### API
```url
http://localhost:8080/swagger-ui.html#/
```

### Run generateHTTP.py (Optional)
```cmd
python generateHTTP.py
```

### Run HTTPClient - populateApp.http (Optional)
Run in Intelliji with Run All command

---

## Tools and Technologies

- [Spring-Boot [2.3.4]](https://spring.io/)
- [Kotlin [1.3.72]](https://kotlinlang.org/)
- [Maven](http://maven.apache.org/)
- [Swagger](https://swagger.io/)
- [IntelliJ IDEA](https://www.jetbrains.com/idea/)

---

## Adicional info

### Git commands

```bash
git pull origin master
git add .
git commit -m "Initial commit"
git push
git rm -r --cached Path/to/directories
```

### Developers

- Rodrigo Lopes - rfa.lopes@campus.fct.unl.pt
- Miguel Teodoro - mt.moreira@campus.fct.unl.pt
- Pedro Guilherme - pg.silva@campus.fct.unl.pt
