import {DataItemDTO, GrantCallDTO} from "../../typescript-fetch-client";
import React from "react";

export const GrantCallApplyForm = (grantCall: GrantCallDTO) => {

    return (
        <div>
            <h1>{grantCall.title}</h1>
            <form>
                {grantCall.dataItems.map((dataItemDTO: DataItemDTO) => (
                    <>
                        <h2>{dataItemDTO.header}</h2>
                        <h4>{dataItemDTO.subTitle}</h4>
                    </>
                ))}
            </form>
            <button type={"submit"}>Apply to Grant</button>
        </div>
    );
};
