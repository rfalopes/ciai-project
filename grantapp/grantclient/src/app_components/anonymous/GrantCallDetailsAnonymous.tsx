import {GrantCallsControllerApiFp, GrantCallSponsorDTO} from "../../typescript-fetch-client";
import React from "react";
import {RouteComponentProps} from 'react-router';
import Loader from 'react-loader-spinner';
import {GoBackButton} from "../GrantApplicationsList";
import {dateFormat} from "../../utils/Utils";
import StatusComponent from "../components/StatusComponent";

interface GrantCallsProps {
    grantCallId: number,
}

type GrantCallListState = {
    grantCallId: number,
    grantCall: GrantCallSponsorDTO
    isFetching: boolean
}

class GrantCallDetailsAnonymous extends React.Component<RouteComponentProps<GrantCallsProps>, GrantCallListState> {

    constructor(props: GrantCallsProps) {
        super(props);
        this.state = {
            grantCallId: this.props.match.params.id,
            grantCall: undefined,
            isFetching: true,
        };
    }

    async componentDidMount() {

        await GrantCallsControllerApiFp().getGrantCallByIdCompleteUsingGET(this.state.grantCallId)()
            .then((result: GrantCallSponsorDTO) => this.setState({
                grantCall: result,
                isFetching: false
            }));
    }

    render() {
        if (this.state.isFetching)
            return (
                <Loader className="center"
                        type="TailSpin"
                        color="#CCC"
                        height={60}
                        width={60}
                        timeout={3000}
                />)
        else {
            return (
                <div>
                    <div className="header">
                        <div>
                            Grant Application Details
                        </div>
                        <div className="top-right mt-1">
                            <StatusComponent status={this.state.grantCall.status.toString()}/>
                        </div>
                    </div>
                    <hr className="hr-style"/>
                    <div className="m-5">
                        <div>
                            <div className="info-title">Sponsor:</div>
                            <div className="info-data">{this.state.grantCall.sponsorName}</div>
                        </div>
                        <div>
                            <div className="info-title">Description:</div>
                            <div className="info-data">{this.state.grantCall.description}</div>
                        </div>
                        <div>
                            <div className="info-title">Start Date:</div>
                            <div className="info-data">{dateFormat(this.state.grantCall.startDate)}</div>
                        </div>
                        <div>
                            <div className="info-title">End Date:</div>
                            <div className="info-data">{dateFormat(this.state.grantCall.expireDate)}</div>
                        </div>
                        <div>
                            <div className="info-title">Funding:</div>
                            <div className="info-data">{this.state.grantCall.funding}€</div>
                        </div>
                    </div>
                    <div className="mr-5">
                        <GoBackButton/>
                    </div>
                </div>
            );
        }
    }
}


export default GrantCallDetailsAnonymous;
