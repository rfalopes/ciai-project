import React, {Component} from "react";
import '../../styles/global_styles.css'
import {Link} from 'react-router-dom';
import {
    GrantCallDTO,
    GrantCallsControllerApiFp,
    GrantCallSponsorDTO
} from "../../typescript-fetch-client";
import {GoBackButton} from "../GrantApplicationsList";
import Loader from 'react-loader-spinner';
import {RouteComponentProps} from 'react-router';
import {dateFormat} from "../../utils/Utils";
import Breadcrumb from "react-bootstrap/Breadcrumb";

export enum GrantCallStatus {
    OPEN = 'open',
    CLOSED = 'close',
    ALL = 'all',
}

type ListState = {
    grantCalls: GrantCallSponsorDTO[]
    searchStatus: GrantCallStatus
}

type ListRows = {
    grantCalls: GrantCallSponsorDTO[]
}

class GrantCallRow extends Component<RouteComponentProps<ListRows>, ListState> {

    render() {

        if(this.props.grantCalls.length === 0)
            return(<h4 className="text-muted d-flex justify-content-center p-5">Nothing to Show ...</h4>)

        return (
            <table className="table">
                <thead className="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Title</th>
                    <th scope="col">Start Date</th>
                    <th scope="col">End Date</th>
                    <th scope="col">Status</th>
                </tr>

                </thead>
                <tbody>
                {this.props.grantCalls.map((grantCall: GrantCallSponsorDTO) => (

                    <tr key={grantCall.id}>
                        <th scope="row">
                            <Link className="no_style" to={"/home/grant_calls/" + grantCall.id}>
                                <h6>{grantCall.id}</h6>
                            </Link>
                        </th>
                        <td>
                            <Link className="no_style" to={"/home/grant_calls/" + grantCall.id}>
                                {grantCall.title}
                            </Link>
                        </td>
                        <td>
                            {dateFormat(grantCall.startDate)}
                        </td>
                        <td>
                            {dateFormat(grantCall.expireDate)}
                        </td>
                        <td>
                            {grantCall.status}
                        </td>
                    </tr>

                ))}

                </tbody>
            </table>
        );
    }
}

interface GrantCallsListProps {
    status: GrantCallStatus,
}

type GrantCallsListState = {
    searchStatus: GrantCallStatus
    grantCalls: GrantCallDTO[]
    isFetching: boolean
}


class AllGrantCalls extends Component<RouteComponentProps<GrantCallsListProps>, GrantCallsListState> {
    constructor(props: GrantCallsListProps) {
        super(props);
        this.state = {
            searchStatus: this.props.location.state.status,
            grantCalls: [],
            isFetching: true
        };
    }

    async componentDidMount() {
        await GrantCallsControllerApiFp().getGrantCallsOnStatusCompleteUsingGET(this.state.searchStatus.toString())
        ().then(result => this.setState({
            grantCalls: result,
            isFetching: false
        }));
    }

    render() {
        if (this.state.isFetching) {
            return (<Loader className="center"
                            type="TailSpin"
                            color="#CCC"
                            height={60}
                            width={60}
                            timeout={3000}
            />)
        } else {
            return (
                <div className="m-5">
                    <h2>All Grant Calls</h2>
                    <Breadcrumb>
                        <Breadcrumb.Item linkAs={Link} linkProps={{to:{pathname:"/home"}}}>Home</Breadcrumb.Item>
                        <Breadcrumb.Item active>All grant calls</Breadcrumb.Item>
                    </Breadcrumb>
                    <GrantCallRow grantCalls={this.state.grantCalls}/>
                    <GoBackButton/>
                </div>
            );
        }
    }
}

export default AllGrantCalls;
