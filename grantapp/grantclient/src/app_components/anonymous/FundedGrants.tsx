import {
    GrantApplicationDetailsDTO,
    GrantCallsControllerApiFp,
} from "../../typescript-fetch-client";
import React from "react";
import {RouteComponentProps} from 'react-router';
import Loader from 'react-loader-spinner';
import {GoBackButton} from "../GrantApplicationsList";
import Breadcrumb from "react-bootstrap/Breadcrumb";
import {Link} from "react-router-dom";
import {GrantCallStatus} from "./AllGrantCalls";

interface GrantCallsProps {
    grantCallId: number,
}

type GrantListState = {
    grantCallId: number,
    grantApplications: GrantApplicationDetailsDTO[]
    isFetching: boolean
    funding: number
}

type ListRows = {
    grantApplications: GrantApplicationDetailsDTO[]
    funding:number
}

class GrantApplicationRow extends React.Component<ListRows, {}> {

    render() {

        if(this.props.grantApplications.length === 0)
            return(<h4 className="text-muted d-flex justify-content-center p-5">Nobody Won Grant application</h4>)

        return (
            <table className="table">
                <thead className="thead-dark">
                <tr>
                    <th>Student</th>
                    <th>Funding</th>
                </tr>

                </thead>
                <tbody>
                {this.props.grantApplications.map((grantApplication: GrantApplicationDetailsDTO) => (
                    <tr key={grantApplication.id}>
                        <td>
                            {grantApplication.studentName}
                        </td>
                        <td>
                            {this.props.funding} €
                        </td>
                    </tr>
                ))}

                </tbody>
            </table>
        );
    }
}

class FundedGrants extends React.Component<RouteComponentProps<GrantCallsProps>, GrantListState> {

    constructor(props: GrantCallsProps) {
        super(props);
        this.state = {
            grantCallId: this.props.match.params.id,
            grantApplications: undefined,
            isFetching: true,
            funding: this.props.location.state.funding

        };
    }

    async componentDidMount() {

        await GrantCallsControllerApiFp().getAcceptedGrantCallApplicationsUsingGET(this.state.grantCallId)()
            .then(result => this.setState({
                    grantApplications: result,
                    isFetching: false
                })
            );
    }

    render() {
        if (this.state.isFetching)
            return (
                <Loader className="center"
                        type="TailSpin"
                        color="#CCC"
                        height={60}
                        width={60}
                        timeout={3000}
                />)
        else {
            //TODO
            return (
                <div className="m-5">
                    <h1>Funded</h1>
                    <Breadcrumb>
                        <Breadcrumb.Item linkAs={Link} linkProps={{to:{pathname:"/home"}}}>Home</Breadcrumb.Item>
                        <Breadcrumb.Item linkAs={Link} linkProps={{to:{pathname:"/home/close_grant_calls", state: {status: GrantCallStatus.CLOSED}}}}>Closed grant calls</Breadcrumb.Item>
                        <Breadcrumb.Item active>Closed grant call {this.state.grantCallId}</Breadcrumb.Item>
                    </Breadcrumb>
                    <GrantApplicationRow grantApplications={this.state.grantApplications} funding={this.state.funding}/>
                    <GoBackButton/>
                </div>
            );
        }
    }
}

export default FundedGrants;
