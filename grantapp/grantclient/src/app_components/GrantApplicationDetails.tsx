import React from "react";
import '../styles/global_styles.css'
import Loader from 'react-loader-spinner'
import {
    DataItemResponseDetailsDTO,
    GrantApplicationDetailsDTO,
    GrantApplicationsControllerApiFp, GrantApplicationStatusEnum,
} from "../typescript-fetch-client";
import {GoBackButton} from "./GrantApplicationsList";
import {Link, Redirect} from "react-router-dom";
import {AccountInfo} from "./LoginPage";
import {RouteComponentProps} from 'react-router';
import {confirmAlert} from "react-confirm-alert";
import StatusComponent from "./components/StatusComponent";

type GrantAppState = {
    student: AccountInfo,
    isFetching: boolean,
    grantApplication: GrantApplicationDetailsDTO
    submissionOk: boolean,
    grantApplicationID: number
}

type DataItems = {
    dataItems: DataItems[]
}

interface GrantAppProps {
    account: AccountInfo
}

class GrantApplicationDetails extends React.Component<RouteComponentProps<GrantAppProps>, GrantAppState> {
    constructor(props) {
        super(props);
        this.state = {
            grantApplicationID: this.props.match.params.id,
            student: this.props.location.state.account,
            isFetching: true,
            grantApplication: undefined,
            submissionOk: false,
        }
    }

    componentDidMount() {
        const grantApplicationId = this.props.match.params.id
        GrantApplicationsControllerApiFp().getGrantApplicationDetailsByIdUsingGET(grantApplicationId, {
            headers: {
                'Authorization': 'Basic ' + btoa(this.state.student.username + ':' + this.state.student.password),
            }
        })().then(result => this.setState(() => ({
                grantApplication: result,
                isFetching: false,
            }))
        )
    }


    render() {

        if (this.state.isFetching) {
            return (
                <Loader className="center" type="TailSpin" color="#CCC" height={60} width={60} timeout={3000}/>
            )
        } else {
            return (

                <div>
                    <div >
                        <div className="header">
                            Grant Application Details
                        </div>
                        <div className="top-right mt-1">
                            <StatusComponent status={this.state.grantApplication.status.toString()}/>
                        </div>
                    </div>
                    <hr className="hr-style"/>

                    <div className="m-5">
                        <div className="info-title"> Grant Call:</div>
                        <div className="info-data">{this.state.grantApplication.grantCallName}</div>
                        <div className="info-title"> Student:</div>
                        <div className="info-data">{this.state.grantApplication.studentName}</div>
                    </div>
                    <div className="m-5 p-3 form-fields">
                        <h4 className="pl-4 pb-3"> Form Fields </h4>
                        <div>
                            {this.state.grantApplication.fieldsResponses.map((fieldResponse: DataItemResponseDetailsDTO) => (
                                <div key={fieldResponse.id}>
                                    <div className="info-title">
                                        {fieldResponse.title}:
                                    </div>
                                    <div className="info-data">
                                        {fieldResponse.value}
                                    </div>
                                </div>
                            ))}
                        </div>
                    </div>
                    <div className="m-5">
                        <BottomButtons submissionOk={this.state.submissionOk} student={this.state.student}
                                       grantApplication={this.state.grantApplication}/>
                        <GoBackButton/>
                    </div>
                </div>
            )
        }
    };
}

class BottomButtons extends React.Component<{ submissionOk: boolean | undefined, student: AccountInfo, grantApplication: GrantApplicationDetailsDTO }, { submissionAccepted: boolean }> {

    constructor(props) {
        super(props);
        this.state = {submissionAccepted: this.props.submissionOk}
    }

    handleGrantApplicationSubmit = () => {
        GrantApplicationsControllerApiFp().submitGrantApplicationUsingPUT(this.props.grantApplication.id, {
            headers: {
                'Authorization': 'Basic ' + btoa(this.props.student.username + ':' + this.props.student.password),
            }
        })().then(() => confirmAlert({
                title: 'Grant Application Submitted',
                message: 'Are you sure you want submit your application?',
                buttons: [
                    {
                        label: 'Yes',
                        onClick: () => this.setState({submissionAccepted: true})
                    },
                    {
                        label: 'Cancel',
                        onClick: () => this.setState({submissionAccepted: undefined})
                    },
                ]
            })
        )
    }

    render() {
        switch (this.props.grantApplication.status) {
            case GrantApplicationStatusEnum.CREATED:
                return (
                    !this.state.submissionAccepted ?
                        <button type="button" className="btn btn-success float-right ml-3"
                                onClick={this.handleGrantApplicationSubmit}>
                            Submit
                        </button>
                        :
                        <Redirect to={{
                            pathname: "/home/student",
                            state: this.props.student
                        }}/>
                );
            case GrantApplicationStatusEnum.SUBMITTED:
                return <div/>;
            default :
                return (<Link className="no_style" to={{
                    pathname: '/grant_application/' + this.props.grantApplication.id + '/reviews',
                    state: {account: this.props.student, callName: this.props.grantApplication.grantCallName}
                }}>
                    <button type="button" className="btn btn-primary float-right ml-3">
                        See Reviews
                    </button>
                </Link>);
        }

    }


}


export default GrantApplicationDetails;
