import React from "react";
import '../../styles/global_styles.css';
import {Link} from 'react-router-dom';
import Breadcrumb from 'react-bootstrap/Breadcrumb'

import {RouteComponentProps} from 'react-router';
import {GoBackButton} from "../GrantApplicationsList";
import {AccountInfo} from "../LoginPage";
import {GrantCallDTO, StudentsControllerApiFp} from "../../typescript-fetch-client";
import Loader from 'react-loader-spinner'

export enum GrantCallStatus {
    OPEN = 'open',
    CLOSED = 'close',
    ALL = 'all',
}

type ListState = {
    grantCalls: GrantCallDTO[]
    searchStatus: GrantCallStatus
}

type GrantCallTablePops = {
    grantCalls: GrantCallDTO[]
    account: AccountInfo
}

const GrantCallTable = (grantCallsProps: {
    grantCalls: GrantCallDTO[],
    accountInfo: AccountInfo
}) => {

    if (grantCallsProps.grantCalls.length == 0) {
        return (<div className="m-5">

                <h4 className="text-muted d-flex justify-content-center p-5">There isn't any new Grant Call ...</h4>
            </div>
        )
    } else {
        return (

            <table className="table">
                <thead className="thead-dark">
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Title</th>
                    <th scope="col">Start Date</th>
                    <th scope="col">End Date</th>
                </tr>

                </thead>
                <tbody>
                {grantCallsProps.grantCalls.map((grantCall: GrantCallDTO) => (

                    <tr key={grantCall.id}>
                        <td scope="row">
                            <Link className="no_style" to={{
                                pathname: '/student/grant_calls/' + grantCall.id,
                                state: {account: grantCallsProps.accountInfo}
                            }}>
                                <h6>{grantCall.id}</h6>
                            </Link>
                        </td>
                        <td>
                            <Link className="no_style" to={{
                                pathname: '/student/grant_calls/' + grantCall.id,
                                state: {account: grantCallsProps.accountInfo}
                            }}>
                                {grantCall.title}
                            </Link>
                        </td>
                        <td>
                            {grantCall.startDate.toString().split("T")[0]}
                        </td>
                        <td>
                            {grantCall.expireDate.toString().split("T")[0]}
                        </td>

                    </tr>


                ))}

                </tbody>
            </table>
        )
    }
}

interface GrantCallsListProps {
    status: GrantCallStatus,
    student: AccountInfo
}

type GrantCallsListState = {
    searchStatus: GrantCallStatus
    student: AccountInfo
    grantCalls: GrantCallDTO[]
    isFetching: boolean
}


class StudentGrantCallsList extends React.Component<RouteComponentProps<GrantCallsListProps>, GrantCallsListState> {
    constructor(props: GrantCallsListProps) {
        super(props);
        this.state = {
            searchStatus: this.props.location.state.status,
            student: this.props.location.state.student,
            grantCalls: [],
            isFetching: true
        };
    }

    async componentDidMount() {
        await StudentsControllerApiFp().getOpenAndNotCreatedGrantCallsUsingGET(this.state.student.accountId, {
            headers: {
                'Authorization': 'Basic ' + btoa(this.state.student.username + ':' + this.state.student.password),
            }
        })().then(result => this.setState(() => ({
            grantCalls: result,
            isFetching: false
        })));
    }

    render() {
        if (this.state.isFetching) {
            return (
                <Loader className="center"
                        type="TailSpin"
                        color="#CCC"
                        height={60}
                        width={60}
                        timeout={3000}
                />
            )
        } else {
            if (this.state.grantCalls !== undefined) {
                return (
                    <div className="m-5">
                        <h1 className="text-dark">Grant Calls</h1>
                        <Breadcrumb>
                            <Breadcrumb.Item linkAs={Link} linkProps={{
                                to: {
                                    pathname: "/home/student",
                                    state: this.state.student
                                }
                            }}>Home</Breadcrumb.Item>
                            <Breadcrumb.Item active>Open Grant Calls</Breadcrumb.Item>
                        </Breadcrumb>
                        <GrantCallTable grantCalls={this.state.grantCalls} accountInfo={this.state.student}/>
                        <GoBackButton/>
                    </div>
                );
            } else {
                return <div></div>
            }
        }
    }
}

export default StudentGrantCallsList;
