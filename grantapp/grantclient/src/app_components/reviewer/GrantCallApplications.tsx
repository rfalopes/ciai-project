import React, {Component} from "react";
import '../../styles/global_styles.css'
import {Link} from 'react-router-dom';
import {
    GrantApplicationDetailsDTO,
    GrantApplicationStatusEnum,
    GrantCallsControllerApiFp
} from "../../typescript-fetch-client";
import {GoBackButton} from "../GrantApplicationsList";
import Loader from 'react-loader-spinner';
import {RouteComponentProps} from 'react-router';
import {AccountInfo} from "../LoginPage";
import Breadcrumb from "react-bootstrap/Breadcrumb";

export enum GrantCallStatus {
    OPEN = 'open',
    CLOSED = 'close',
    ALL = 'all',
}

type ListRows = {
    account: AccountInfo
    granApplications: GrantApplicationDetailsDTO[]
}

class GrantCallApplications extends Component<RouteComponentProps<ListRows>, {}> {

    render() {

        if(this.props.granApplications.length === 0)
            return(<h4 className="text-muted d-flex justify-content-center p-5">Nothing to Show ...</h4>)

        return(
            <table className="table">
                <thead className="thead-dark">
                <tr>
                    <th scope="col">Grant Application Id</th>
                    <th scope="col">Candidate</th>
                </tr>
                </thead>
                <tbody>
                {this.props.granApplications.map((grantApplication: GrantApplicationDetailsDTO) => (

                    <tr key={grantApplication.id}>
                        <th scope="row">
                            <Link className="no_style" to={{
                                pathname: '/reviewer/grant_application/' + grantApplication.id,
                                state: {reviewer: this.props.account}
                            }}>
                                {grantApplication.id}
                            </Link>
                        </th>
                        <td>
                            {grantApplication.studentName}
                        </td>
                    </tr>

                ))}

                </tbody>
            </table>
        );
    }
}

interface GrantCallsListProps {
    reviewer: AccountInfo,

}

type GrantCallsListState = {
    grantCallId: number
    reviewer: AccountInfo,
    grantApplications: GrantApplicationDetailsDTO[]
    isFetching: boolean
}


class GrantCallApplicationsList extends Component<RouteComponentProps<GrantCallsListProps>, GrantCallsListState> {
    constructor(props: GrantCallsListProps) {
        super(props);
        this.state = {
            grantCallId : this.props.match.params.id,
            reviewer: this.props.location.state.reviewer,
            grantApplications: [],
            isFetching: true
        };
    }

    async componentDidMount() {
        await GrantCallsControllerApiFp().getGrantCallApplicationsByStatusUsingGET(this.state.grantCallId, GrantApplicationStatusEnum.INVOTING, {
            headers: {
                'Authorization': 'Basic ' + btoa(this.state.reviewer.username + ':' + this.state.reviewer.password),
            }
        })
        ().then(result => this.setState({
            grantApplications: result,
            isFetching: false
        }));
    }

    render() {
        if (this.state.isFetching)
            return (<Loader className="center"
                            type="TailSpin"
                            color="#CCC"
                            height={60}
                            width={60}
                            timeout={3000}
            />)

        return (
            <div className="">
                <div className="m-5">
                    <h1>Grant Applications</h1>
                    <Breadcrumb>
                        <Breadcrumb.Item linkAs={Link} linkProps={{to:{pathname:"/home/reviewer", state: this.state.reviewer}}}>Home</Breadcrumb.Item>
                        <Breadcrumb.Item linkAs={Link} linkProps={{to:{pathname:"/reviewer/grants_for_review", state: {reviewer: this.state.reviewer}}}}>Grant Calls</Breadcrumb.Item>
                        <Breadcrumb.Item active>Grant Call {this.state.grantCallId}</Breadcrumb.Item>
                    </Breadcrumb>
                    <GrantCallApplications granApplications={this.state.grantApplications} account={this.state.reviewer}/>
                    <GoBackButton/>
                </div>
            </div>
        );
    }
}

export default GrantCallApplicationsList;
