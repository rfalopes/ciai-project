import React from "react";
import '../../styles/global_styles.css'
import Loader from 'react-loader-spinner'
import {GoBackButton} from "../GrantApplicationsList";
import {StudentsControllerApiFp, StudentWithoutPasswordDTO} from "../../typescript-fetch-client";
import {RouteComponentProps} from 'react-router';
import {AccountInfo} from "../LoginPage";
import {dateFormat} from "../../utils/Utils";

type GrantAppState = {
    reviewer: AccountInfo,
    studentId: number,
    isFetching: boolean
    student: StudentWithoutPasswordDTO
}

type DataItems = {
    dataItems: DataItems[]
}

class StudentDetails extends React.Component<RouteComponentProps<AccountInfo>, GrantAppState> {
    constructor(props) {
        super(props);
        this.state = {
            reviewer: this.props.location.state.account,
            studentId: this.props.match.params.id,
            isFetching: true,
            student: undefined,
        }
    }

    componentDidMount() {
        StudentsControllerApiFp().getStudentByIdUsingGET(this.state.studentId, {
            headers: {
                'Authorization': 'Basic ' + btoa(this.state.reviewer.username + ':' + this.state.reviewer.password),
            }
        })().then(result => this.setState(() => ({
                student: result,
                isFetching: false,
            }))
        )
    }

    render() {

        if (this.state.isFetching) {
            return (
                <Loader className="center" type="TailSpin" color="#CCC" height={60} width={60} timeout={3000}/>
            )
        } else {
            return (

                <div>
                    <div>
                        <div className="header">
                            Student Details
                        </div>
                        <div className="top-right">
                            Student Id: {this.state.student.id}
                        </div>
                    </div>
                    <hr className="hr-style"/>
                    <div>
                        <div className="info-title"> Username:</div>
                        <div className="info-data">{this.state.student.userName}</div>
                        <div className="info-title"> Full Name:</div>
                        <div className="info-data">{this.state.student.name}</div>
                        <div className="info-title"> Email:</div>
                        <div className="info-data">{this.state.student.email}</div>
                        <div className="info-title"> BirthDate:</div>
                        <div className="info-data">{dateFormat(this.state.student.birthDate)}</div>
                        <div className="info-title"> Average</div>
                        <div className="info-data">{this.state.student.average}</div>
                    </div>
                    <div className="m-5">
                        <button type="button" className="btn btn-primary float-left">
                            See Student CV
                        </button>
                        <GoBackButton/>
                    </div>
                </div>
            )
        }
    };


}


export default StudentDetails;
