import React from "react";
import {Link} from 'react-router-dom';
import {AccountInfo} from "../LoginPage";
import {RouteComponentProps} from 'react-router';
import {FcCollaboration, FcComments} from "react-icons/fc";

class ReviewerHomePage extends React.Component<RouteComponentProps<AccountInfo>, AccountInfo> {

    constructor(props) {
        super(props);
        this.state = this.props.location.state
    }

    render() {
        return (
            <div  className="m-5">
                <div className="header bg-dark rounded text-light p-4">
                    Reviewer: {this.state.name}
                    <Link to="/home" className="btn btn-secondary float-right">Logout</Link>
                </div>
                <div className="image-group">
                    <Link to={{
                        pathname: "/reviewer/grants_for_review",
                        state: {reviewer: this.state}
                    }}>
                        <div className="image-block btn-dark m-4 rounded shadow w-25">
                            <FcComments size={100}/>
                            <h3 className="text-light">Reviewer Grant Calls</h3>
                        </div>
                    </Link>
                    <Link to={{
                        pathname: "/reviewer/chaired_grants",
                        state: {reviewer: this.state}
                    }}>
                        <div className="image-block btn-dark m-4 rounded shadow w-25">
                            <FcCollaboration size={100}/>
                            <h3 className="text-light">Panel Chaired Grant Calls</h3>
                        </div>
                    </Link>
                </div>
            </div>
        );
    }
}

export default ReviewerHomePage;
