import React, {Component} from "react";
import '../../styles/global_styles.css'
import {Link} from 'react-router-dom';
import {
    GrantCallDTO,
    GrantCallSponsorDTO,
    ReviewersControllerApiFp
} from "../../typescript-fetch-client";
import {GoBackButton} from "../GrantApplicationsList";
import Loader from 'react-loader-spinner';
import {RouteComponentProps} from 'react-router';
import {dateFormat} from "../../utils/Utils";
import {AccountInfo} from "../LoginPage";
import Breadcrumb from "react-bootstrap/Breadcrumb";

export enum GrantCallStatus {
    OPEN = 'open',
    CLOSED = 'close',
    ALL = 'all',
}

type ListRows = {
    account: AccountInfo
    grantCalls: GrantCallSponsorDTO[]
}

class GrantCallsToReviewTable extends Component<RouteComponentProps<ListRows>, {}> {

    render() {
        if(this.props.grantCalls.length === 0)
            return(<h4 className="text-muted d-flex justify-content-center p-5">Nothing to Review ...</h4>)

        return (
            <table className="table">
                <thead className="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Title</th>
                    <th scope="col">Start Date</th>
                    <th scope="col">End Date</th>
                </tr>
                </thead>
                <tbody>
                {this.props.grantCalls.map((grantCall: GrantCallSponsorDTO) => (

                    <tr key={grantCall.id}>
                        <th scope="row">
                            <Link className="no_style" to={{
                                pathname: '/reviewer/grant_calls/' + grantCall.id + '/grant_applications/',
                                state: {reviewer: this.props.account}
                            }}>
                                <h6>{grantCall.id}</h6>
                            </Link>
                        </th>
                        <td>
                            <Link className="no_style" to={{
                                pathname: '/reviewer/grant_calls/' + grantCall.id + '/grant_applications/',
                                state: {reviewer: this.props.account}
                            }}>
                                {grantCall.title}
                            </Link>
                        </td>
                        <td>
                            {dateFormat(grantCall.startDate)}
                        </td>
                        <td>
                            {dateFormat(grantCall.expireDate)}
                        </td>
                    </tr>

                ))}

                </tbody>
            </table>
        );
    }
}

interface GrantCallsListProps {
    reviewer: AccountInfo,
}

type GrantCallsListState = {
    reviewer: AccountInfo,
    grantCalls: GrantCallDTO[]
    isFetching: boolean
}


class GrantCallsToReview extends Component<RouteComponentProps<GrantCallsListProps>, GrantCallsListState> {
    constructor(props: GrantCallsListProps) {
        super(props);
        this.state = {
            reviewer: this.props.location.state.reviewer,
            grantCalls: [],
            isFetching: true
        };
    }

    async componentDidMount() {
        await ReviewersControllerApiFp().getAllGrantCallsToEvaluationUsingGET(this.state.reviewer.accountId, {
            headers: {
                'Authorization': 'Basic ' + btoa(this.state.reviewer.username + ':' + this.state.reviewer.password),
            }
        })
        ().then(result => this.setState({
            grantCalls: result,
            isFetching: false
        }));
    }

    render() {
        if (this.state.isFetching) {
            return (<Loader className="center"
                            type="TailSpin"
                            color="#CCC"
                            height={60}
                            width={60}
                            timeout={3000}
            />)
        } else {

            return (
                <div className="m-5">
                    <h1>Reviewer Grant Calls</h1>
                    <Breadcrumb>
                        <Breadcrumb.Item linkAs={Link} linkProps={{to:{pathname:"/home/reviewer", state: this.state.reviewer}}}>Home</Breadcrumb.Item>
                        <Breadcrumb.Item active>Grant Calls</Breadcrumb.Item>
                    </Breadcrumb>
                    <GrantCallsToReviewTable
                        account={this.state.reviewer}
                        grantCalls={this.state.grantCalls}
                    />
                    <GoBackButton/>
                </div>
            );
        }
    }
}

export default GrantCallsToReview;
