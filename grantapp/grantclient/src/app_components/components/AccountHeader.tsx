import React from "react";
import Button from "./buttons/Button";

const AccountHeader = ({username}) => {
    return (
        <div className="bg-light p-3 rounded">
            <h3 className="text-dark float-left"> Hey {username}</h3>
            <LogoutButton/>
        </div>
    );
}

const LogoutButton = () => {
    return (
        <div className="row align-items-end justify-content-end btn-toolbar">
            <Button toPage="/" text="Logout"/>
        </div>
    );
}

export default AccountHeader;
