import {Link} from "react-router-dom";
import React from "react";

const Component = ({name, text, Icon, pathname, state, status}) => {
    return (
        <>
            <div className="image-block bg-light m-4 rounded shadow w-25 text-center">
                <Link to={{
                    pathname: pathname,
                    state: {
                        accountInfo: state,
                        status: status
                    }
                }}>
                    <div className="d-flex justify-content-center align-content-start m-4">
                        <Icon size={100}/>
                    </div>
                    <h3 className="text-muted">{name}</h3>
                </Link>
                <h5 className="m-5 text-success ce">{text}</h5>
            </div>
        </>
    );
}

export default Component;
