import React from "react";

const Footer = () => {
    return (
        <div className="fixed-bottom p-3 rounded text-center">
            <h5 className="text-light">Rodrigo Lopes | Pedro Silva | Miguel Moreira</h5>
        </div>
    );
}

export default Footer;
