import React from "react";
import {GiBookAura} from "react-icons/gi";
import {Button} from "../buttons/Button";

export const Header = () => {
    return (
        <div className="bg-light p-3 rounded">
            <Logo/>
            <LoginButtons/>
        </div>
    );
}

const Logo = () => {
    return (
        <div className="float-left">
            <GiBookAura size={35} color={'dark'}/>
        </div>
    );
}

const LoginButtons = () => {
    return (
        <div className="row align-items-end justify-content-end btn-toolbar">
            <Button toPage="/login" text="Login"/>
            <Button toPage="/sigin" text="Sign in"/>
        </div>
    );
}

export default Header;

