import React from "react";

class StatusComponent extends React.Component<{ status: string }, {}> {
    render() {
        switch (this.props.status) {
            case 'CLOSED':
                return (
                    <div>
                        <div className="text-secondary float-right mt-1 pl-2">
                            {this.props.status}
                        </div>
                        <div className="float-right mt-1">
                            Status:
                        </div>
                    </div>

                );
            case 'OPEN':
                return (
                    <div>
                        <div className="text-primary float-right mt-1 pl-2">
                            {this.props.status}
                        </div>
                        <div className="float-right mt-1">
                            Status:
                        </div>
                    </div>
                );
            case 'CREATED':
                return (
                    <div>
                        <div className="text-primary float-right mt-1 pl-2">
                            {this.props.status}
                        </div>
                        <div className="float-right mt-1">
                            Status:
                        </div>
                    </div>
                );
            case 'SUBMITTED':
                return (
                    <div>
                        <div className="text-primary float-right mt-1 pl-2">
                            {this.props.status}
                        </div>
                        <div className="float-right mt-1">
                            Status:
                        </div>
                    </div>
                );
            case 'IN_VOTING':
                return (
                    <div>
                        <div className="text-warning float-right mt-1 pl-2">
                            {this.props.status}
                        </div>
                        <div className="float-right mt-1">
                            Status:
                        </div>
                    </div>
                );
            case 'ACCEPTED':
                return (
                    <div>
                        <div className="text-success float-right mt-1 pl-2">
                            {this.props.status}
                        </div>
                        <div className="float-right mt-1">
                            Status:
                        </div>
                    </div>
                );
            case 'REJECTED':
                return (
                    <div>
                        <div className="text-danger float-right mt-1 pl-2">
                            {this.props.status}
                        </div>
                        <div className="float-right mt-1">
                            Status:
                        </div>

                    </div>
                );
        }

    }
}

export default StatusComponent;
