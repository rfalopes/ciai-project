import {Link} from "react-router-dom";
import React from "react";

export const Button = ({ text, toPage }) => {
    return (
        <div className="btn-group mr-3">
            <Link to={toPage} className="btn btn-secondary float-right">{ text }</Link>
        </div>
    );
}

export default Button;
