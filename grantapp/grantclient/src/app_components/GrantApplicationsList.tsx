import React from "react";
import '../styles/global_styles.css'
import {Link,} from 'react-router-dom';
import {RouteComponentProps} from 'react-router';
import {
    GrantApplicationForListingDTO,
    GrantApplicationStatusEnum,
    StudentsControllerApiFp
} from "../typescript-fetch-client";
import Loader from 'react-loader-spinner'
import {AccountInfo} from "./LoginPage";
import {useHistory} from 'react-router-dom';
import Breadcrumb from "react-bootstrap/Breadcrumb";


type ListState = {
    grantApplications: GrantApplicationForListingDTO[]
    searchStatus?: GrantApplicationStatusEnum
    student: AccountInfo
    isFetching: boolean
}

type ListRows = {
    grantApplications: GrantApplicationForListingDTO[]
    account: AccountInfo
}

class GrantApplicationRow extends React.Component<ListRows, {}> {

    render() {

        if(this.props.grantApplications.length === 0)
            return(<h4 className="text-muted d-flex justify-content-center p-5">Nothing to Show ...</h4>)

        return (
            <table className="table">
                <thead>
                <tr>
                    <th>Grant Applications</th>
                </tr>

                </thead>
                <tbody>
                {this.props.grantApplications.map((grantApplication: GrantApplicationForListingDTO) => (
                    <tr key={grantApplication.id}>
                        <td>
                            <Link className="no_style" to={{
                                pathname: '/student/grant_application/' + grantApplication.id,
                                state: {account: this.props.account}
                            }}>{grantApplication.grantCallName}</Link>
                        </td>
                    </tr>
                ))}

                </tbody>
            </table>
        );
    }
}

interface GrantAppProps {
    status: GrantApplicationStatusEnum,
    student: AccountInfo
}

class GrantApplicationsList extends React.Component<RouteComponentProps<GrantAppProps>, ListState> {


    constructor(props) {
        super(props);
        this.state = {
            //tasks remains unchanged
            searchStatus: this.props.location.state.status,
            student: this.props.location.state.student,
            grantApplications: [],
            isFetching: true
        }

    }

    async componentDidMount() {
        await StudentsControllerApiFp().getStudentGrantApplicationsUsingGET(this.state.student.accountId, this.state.searchStatus, {
            headers: {
                'Authorization': 'Basic ' + btoa(this.state.student.username + ':' + this.state.student.password),
            }
        })().then(result => this.setState(() => ({
                grantApplications: result,
                isFetching: false
            }))
        );
    }

    render() {
        if (this.state.isFetching) {
            return (
                <Loader className="center"
                        type="TailSpin"
                        color="#CCC"
                        height={60}
                        width={60}
                        timeout={3000}
                />
            )
        } else {

            return (
                <div className="m-5">
                    <h1 className="text-dark">Grant Applications</h1>
                    <Breadcrumb>
                        <Breadcrumb.Item linkAs={Link} linkProps={{to:{pathname:"/home/student", state: this.state.student}}}>Home</Breadcrumb.Item>
                        <Breadcrumb.Item active>Grant Applications</Breadcrumb.Item>
                    </Breadcrumb>
                    <GrantApplicationRow grantApplications={this.state.grantApplications}
                                         account={this.state.student}/>
                    <GoBackButton/>
                </div>
            );
        }
    }
}

export const GoBackButton = () => {
    const history = useHistory()
    return (
        <button type="button" className="btn btn-dark float-right" onClick={() => history.goBack()}>
            Go Back
        </button>
    )
}

export default GrantApplicationsList;
