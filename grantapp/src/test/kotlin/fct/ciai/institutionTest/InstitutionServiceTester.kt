package fct.ciai.institutionTest

import fct.ciai.repositories.institutions.InstitutionRepository
import fct.ciai.services.ConflictException
import fct.ciai.services.NotFoundException
import fct.ciai.services.institutions.InstitutionsServices
import fct.ciai.utils.ObjectsDAO
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit4.SpringRunner
import java.util.*


@RunWith(SpringRunner::class)
@SpringBootTest
class InstitutionServiceTester {

    @Autowired
    lateinit var institutionsServices : InstitutionsServices

    @MockBean
    lateinit var institutionRepository : InstitutionRepository

    companion object{
        private val institution1DAO = ObjectsDAO.institution1DAO
        private val institution2DAO = ObjectsDAO.institution2DAO
    }

    @Test
    fun `Test GET One Institution with Mocked Repository (No Error)`() {
        Mockito.`when`(institutionRepository.findById(1L)).thenReturn(Optional.of(institution1DAO))
        assertEquals(institutionsServices.getInstitutionById(1L), institution1DAO)
    }

    @Test(expected = NotFoundException::class)
    fun `Test GET One Institution with Mocked Repository (Not Found Error)`() {
        Mockito.`when`(institutionRepository.findById(1L)).thenReturn(Optional.empty())
        institutionsServices.getInstitutionById(1L)
    }

    @Test
    fun `Test GET All Institution with Mocked Repository (No Error)`() {
        val institutions = listOf(institution1DAO, institution2DAO)
        Mockito.`when`(institutionRepository.findAll())
                .thenReturn(institutions)
        assertEquals(institutionsServices.getAllInstitutions(), institutions)
    }

    @Test
    fun `Test CREATE Institution with Mocked Repository (No Error)`() {
        Mockito.`when`(institutionRepository.findById(institution1DAO.id))
                .thenReturn(Optional.empty())
        institutionsServices.addInstitution(institution1DAO)
    }

    @Test(expected = ConflictException::class)
    fun `Test CREATE Institution with Mocked Repository (Conflict Error)`() {
        Mockito.`when`(institutionRepository.existsByNameEqualsOrEmailEquals(institution1DAO.name, institution1DAO.email))
                .thenReturn(true)
        institutionsServices.addInstitution(institution1DAO)
    }

    @Test
    fun `Test DELETE Institution with Mocked Repository (No Error)`() {
        Mockito.`when`(institutionRepository.findById(institution1DAO.id))
                .thenReturn(Optional.of(institution1DAO))
        institutionsServices.deleteInstitution(institution1DAO.id)
    }

    @Test(expected = NotFoundException::class)
    fun `Test DELETE Institution with Mocked Repository (Not Found Error)`() {
        Mockito.`when`(institutionRepository.findById(institution1DAO.id))
                .thenReturn(Optional.empty())
        institutionsServices.deleteInstitution(institution1DAO.id)
    }
}
