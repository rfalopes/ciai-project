package fct.ciai.sponsorTest

import fct.ciai.daos.SponsorDAO
import fct.ciai.repositories.sponsor.SponsorRepository
import fct.ciai.repositories.users.AccountRepository
import fct.ciai.services.ConflictException
import fct.ciai.services.NotFoundException
import fct.ciai.services.sponsors.SponsorsServices
import fct.ciai.utils.ObjectsDAO
import org.h2.store.Page
import org.junit.Test
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.data.domain.PageRequest
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.junit4.SpringRunner
import java.util.*


@RunWith(SpringRunner::class)
@SpringBootTest
class SponsorServiceTester {

    @Autowired
    lateinit var sponsorsServices: SponsorsServices

    @MockBean
    lateinit var accountRepository: AccountRepository

    @MockBean
    lateinit var sponsorRepository : SponsorRepository


    companion object{
        private val sponsor1DAO = ObjectsDAO.sponsor1DAO
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET All Sponsors (Non EmptyList)`() {
        Mockito.`when`(sponsorRepository.findAll()).thenReturn(listOf(sponsor1DAO))
        assertEquals(sponsorsServices.getAllSponsors(), listOf(sponsor1DAO))
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET All Sponsors (EmptyList)`() {
        Mockito.`when`(sponsorRepository.findAll()).thenReturn(emptyList())
        assertEquals(sponsorsServices.getAllSponsors(), emptyList<SponsorDAO>())
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET Sponsor By ID (Success)`() {
        Mockito.`when`(sponsorRepository.findById(sponsor1DAO.id)).thenReturn(Optional.of(sponsor1DAO))
        assertEquals(sponsorsServices.getSponsorById(sponsor1DAO.id), sponsor1DAO)
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET Sponsor By ID (Not Found)`() {
        Mockito.`when`(sponsorRepository.findById(sponsor1DAO.id)).thenThrow(NotFoundException(""))
        assertThrows(NotFoundException::class.java) { sponsorsServices.getSponsorById(sponsor1DAO.id) }
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test CREATE Sponsor (Success)`() {
        Mockito.`when`(accountRepository.existsByUserNameEqualsOrEmailEquals(sponsor1DAO.userName, sponsor1DAO.email)).thenReturn(false)
        Mockito.`when`(sponsorRepository.save(sponsor1DAO)).thenReturn(sponsor1DAO)
        assertEquals(sponsorsServices.addSponsor(sponsor1DAO), sponsor1DAO)
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test CREATE Sponsor (Conflict)`() {
        Mockito.`when`(accountRepository.existsByUserNameEqualsOrEmailEquals(sponsor1DAO.userName, sponsor1DAO.email)).thenReturn(true)
        assertThrows(ConflictException::class.java) { sponsorsServices.addSponsor(sponsor1DAO) }
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test DELETE Sponsor (Success)`() {
        Mockito.`when`(sponsorRepository.findById(sponsor1DAO.id)).thenReturn(Optional.of(sponsor1DAO))
        sponsorsServices.deleteSponsor(sponsor1DAO.id)
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test DELETE Sponsor (Not Found)`() {
        Mockito.`when`(sponsorRepository.findById(sponsor1DAO.id)).thenThrow(NotFoundException(""))
        assertThrows(NotFoundException::class.java) { sponsorsServices.deleteSponsor(sponsor1DAO.id) }
    }

}
