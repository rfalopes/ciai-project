package fct.ciai.utils

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.springframework.test.web.servlet.MvcResult

class Utils {

    companion object {

        val mapper = ObjectMapper()

        inline fun <reified T> toObject(result: MvcResult): T {
            val responseString = result.response.contentAsString
            return mapper.readValue(responseString)
        }

        fun <T> toJson(dto: T) : String {
            val ow = mapper.writer().withDefaultPrettyPrinter()
            return ow.writeValueAsString(dto)
        }
    }

}
