package fct.ciai.utils

import fct.ciai.daos.*
import fct.ciai.enums.ReviewStatus
import fct.ciai.enums.RolesEnum
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import java.util.*

class ObjectsDAO {
    companion object {

        fun <T> getEmptyList(): List<T> = emptyList()

        val genericEmptyList = emptyList<ObjectsDAO>()

        val institution1DAO = InstitutionDAO(
                1,
                "institution1",
                "institution1",
                "institution1",
                "",
                mutableListOf()
        )

        val institution2DAO = InstitutionDAO(
                2,
                "institution2",
                "institution2",
                "institution2",
                "",
                mutableListOf()
        )

        val sponsor1DAO = SponsorDAO(
                3,
                "sponsor1",
                "sponsor1",
                "sponsor1",
                "sponsor1",
                "112"
        )

        val sponsor2DAO = SponsorDAO(
                4,
                "sponsor2",
                "sponsor2",
                "sponsor2",
                "sponsor2",
                "112"
        )

        val reviewer1DAO = ReviewerDAO(
                5,
                "reviewer1",
                "reviewer1",
                "reviewer1",
                "reviewer1",
                Date(),
                institution1DAO
        )

        val reviewer2DAO = ReviewerDAO(
                6,
                "reviewer2",
                "reviewer2",
                "reviewer2",
                "reviewer2",
                Date(),
                institution1DAO
        )

        val evaluationPanelDAO = EvaluationPanelDAO(
                7,
                reviewer1DAO,
                mutableListOf()
        )

        val accountStudentRoleDAO = AccountDAO(
                8,
                "student",
                "student",
                BCryptPasswordEncoder().encode("password"),
                "student",
                mutableListOf(RolesEnum.ROLE_STUDENT)
        )

        val accountReviewerRoleDAO = AccountDAO(
                8,
                "reviewer",
                "reviewer",
                BCryptPasswordEncoder().encode("password"),
                "reviewer",
                mutableListOf(RolesEnum.ROLE_REVIEWER)
        )

        val student1DAO = StudentDAO(
                8,
                "student1",
                "student1",
                "student1",
                "student1",
                Date(),
                institution1DAO,
                16.9
        )

        val student2DAO = StudentDAO(
                9,
                "student2",
                "student2",
                "student2",
                "student2",
                Date(),
                institution1DAO,
                18.3
        )

        val grantCallDAO = GrantCallDAO(
                10,
                "grantCall",
                "grantCall",
                Date(),
                Date(),
                1000,
                evaluationPanelDAO,
                mutableListOf(),
                sponsor1DAO
        )

        val grantApplicationDAO = GrantApplicationDAO(
                11,
                grantCallDAO,
                student1DAO
        )

        val evaluationPanelWithReviewer1DAO = EvaluationPanelDAO(
                12,
                reviewer1DAO,
                mutableListOf(reviewer1DAO)
        )

        val grantCallWithReviewer1DAO = GrantCallDAO(
                13,
                "grantCall",
                "grantCall",
                Date(),
                Date(),
                1000,
                evaluationPanelWithReviewer1DAO,
                mutableListOf(),
                sponsor1DAO
        )

        val grantApplicationWithReviewer1DAO = GrantApplicationDAO(
                14,
                grantCallWithReviewer1DAO,
                student1DAO
        )

        val dataItem1DAO = DataItemDAO(
                15,
                "dataItemDAO2",
                "dataItemDAO2",
                "dataItemDAO2",
                false,
                grantCallDAO
        )

        val dataItem2MandatoryDAO = DataItemDAO(
                16,
                "dataItemDAO2",
                "dataItemDAO2",
                "dataItemDAO2",
                true,
                grantCallDAO
        )

        val review1DAO = ReviewDAO(
                17,
                20,
                "Muito bom",
                reviewer1DAO,
                grantApplicationDAO,
                ReviewStatus.NORMAL
        )

        var dataItemResponse1DAO = DataItemResponseDAO(
                0,
                "Pedro",
                dataItem1DAO,
                grantApplicationWithReviewer1DAO
        )

        val grantCallDAO2close = GrantCallDAO(
                10,
                "grantCall expired",
                "grantCall",
                Date(2018),
                Date(2019),
                1000,
                evaluationPanelDAO,
                mutableListOf(),
                sponsor1DAO
        )

        val grantCallDAO3open = GrantCallDAO(
                10,
                "grantCall open",
                "grantCall",
                Date(2019),
                Date(2021),
                1000,
                evaluationPanelDAO,
                mutableListOf(),
                sponsor1DAO
        )

    }
}
