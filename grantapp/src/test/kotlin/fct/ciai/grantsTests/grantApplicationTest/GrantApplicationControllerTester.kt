package fct.ciai.grantsTests.grantApplicationTest

import fct.ciai.dtos.grantApplications.GrantApplicationDTO
import fct.ciai.dtos.ReviewDTO
import fct.ciai.services.NotFoundException
import fct.ciai.services.grants.grantsApplications.GrantApplicationsServices
import fct.ciai.services.grants.grantsCalls.GrantCallsServices
import fct.ciai.services.users.students.StudentsServices
import fct.ciai.utils.ObjectsDAO
import fct.ciai.utils.Utils
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.data.domain.PageRequest
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class GrantApplicationControllerTester {
    @Autowired
    lateinit var mvc: MockMvc

    @MockBean
    lateinit var grantApplicationsService: GrantApplicationsServices

    @MockBean
    lateinit var grantCallsService: GrantCallsServices

    @MockBean
    lateinit var studentsServices: StudentsServices

    companion object {
        const val GRANT_APPLICATIONS_URL = "/grant_applications"

        private val grantApplicationDAO = ObjectsDAO.grantApplicationDAO

        private val grantApplicationDTO = GrantApplicationDTO(grantApplicationDAO)

        private val review1DAO = ObjectsDAO.review1DAO

        private val review1DTO = ReviewDTO(review1DAO)
    }

    //Get Grant application by id
    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET Grant Application By Id (Success)`() {
        Mockito.`when`(grantApplicationsService.getGrantApplicationById(grantApplicationDAO.id))
                .thenReturn(grantApplicationDAO)

        val result = mvc.perform(MockMvcRequestBuilders.get("${GRANT_APPLICATIONS_URL}/${grantApplicationDAO.id}"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andReturn()

        val response = Utils.toObject<GrantApplicationDTO>(result)
        assertEquals(grantApplicationDTO, response )
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET Grant Application By Id (Not found)`() {
        Mockito.`when`(grantApplicationsService.getGrantApplicationById(grantApplicationDAO.id))
                .thenThrow(NotFoundException("Application not found"))

        mvc.perform(MockMvcRequestBuilders.get("${GRANT_APPLICATIONS_URL}/${grantApplicationDAO.id}"))
                .andExpect(MockMvcResultMatchers.status().isNotFound)
                .andReturn()

    }

    //Get all Grant applications
    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET All Grant Applications (Empty List)`() {
        Mockito.`when`(grantApplicationsService.getAllGrantApplications(PageRequest.of(0, 10)))
                .thenReturn(listOf())

        val result = mvc.perform(MockMvcRequestBuilders.get(GRANT_APPLICATIONS_URL))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andReturn()

        val response = Utils.toObject<Iterable<GrantApplicationDTO>>(result)
        assertEquals(listOf<GrantApplicationDTO>() ,response )
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET All Grant Applications (Non Empty List)`() {
        Mockito.`when`(grantApplicationsService.getAllGrantApplications(PageRequest.of(0, 10)))
                .thenReturn(listOf(grantApplicationDAO))

        val result = mvc.perform(MockMvcRequestBuilders.get(GRANT_APPLICATIONS_URL))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andReturn()

        val response = Utils.toObject<Iterable<GrantApplicationDTO>>(result)
        assertEquals(listOf(grantApplicationDTO) ,response )
    }


    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test CREATE Grant Application  (Success)`() {
        Mockito.`when`(grantCallsService.getGrantCallById(grantApplicationDTO.grantCallId))
                .thenReturn(ObjectsDAO.grantCallDAO)
        Mockito.`when`(studentsServices.getStudentById(grantApplicationDTO.studentID))
                .thenReturn(ObjectsDAO.student1DAO)
        mvc.perform(MockMvcRequestBuilders.post(GRANT_APPLICATIONS_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(Utils.toJson(grantApplicationDTO)))
                .andExpect(MockMvcResultMatchers.status().isCreated)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test DELETE Grant Application  (Success)`() {
        Mockito.doNothing().`when`(grantApplicationsService).deleteGrantApplication(grantApplicationDTO.id)

        mvc.perform(delete("${GRANT_APPLICATIONS_URL}/${grantApplicationDAO.id}"))
                .andExpect(MockMvcResultMatchers.status().isNoContent)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test DELETE Grant Application  (Not found)`() {
        Mockito.`when`(grantApplicationsService.deleteGrantApplication(grantApplicationDAO.id)).thenThrow(NotFoundException(""))

        mvc.perform(delete("${GRANT_APPLICATIONS_URL}/${grantApplicationDAO.id}"))
                .andExpect(MockMvcResultMatchers.status().isNotFound)
                .andReturn()
    }

    // Get ALL Grant Application Reviews by ID
    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET All Grant Applications Reviews by ID (Success)`() {
        Mockito.`when`(grantApplicationsService.getAllGrantApplicationReviews(grantApplicationDAO.id))
                .thenReturn(mutableListOf(review1DAO))

        val result = mvc.perform(MockMvcRequestBuilders.get("${GRANT_APPLICATIONS_URL}/${grantApplicationDAO.id}/reviews"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andReturn()

        val response = Utils.toObject<Iterable<ReviewDTO>>(result)
        assertEquals(listOf(review1DTO) ,response )
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET All Grant Applications Reviews by ID (Empty List)`() {
        Mockito.`when`(grantApplicationsService.getAllGrantApplicationReviews(grantApplicationDAO.id))
                .thenReturn(mutableListOf())

        val result = mvc.perform(MockMvcRequestBuilders.get("${GRANT_APPLICATIONS_URL}/${grantApplicationDAO.id}/reviews"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andReturn()

        val response = Utils.toObject<Iterable<ReviewDTO>>(result)
        assertEquals(listOf<ReviewDTO>(), response)
    }
    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET All Grant Applications Reviews by ID (Not Found)`() {
        Mockito.`when`(grantApplicationsService.getAllGrantApplicationReviews(grantApplicationDAO.id))
                .thenThrow(NotFoundException(""))

        mvc.perform(MockMvcRequestBuilders.get("${GRANT_APPLICATIONS_URL}/${grantApplicationDAO.id}/reviews"))
                .andExpect(MockMvcResultMatchers.status().isNotFound)
                .andReturn()
    }
}

