package fct.ciai.grantsTests.grantApplicationTest

import fct.ciai.enums.GrantApplicationStatus
import fct.ciai.repositories.grants.DataItemResponseRepository
import fct.ciai.repositories.grants.GrantApplicationRepository
import fct.ciai.services.ConflictException
import fct.ciai.services.NotFoundException
import fct.ciai.services.grants.grantsApplications.GrantApplicationsServices
import fct.ciai.utils.ObjectsDAO
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit4.SpringRunner
import java.util.*


@RunWith(SpringRunner::class)
@SpringBootTest
class GrantApplicationServiceTester {
    @Autowired
    lateinit var grantApplicationsServices: GrantApplicationsServices

    @MockBean
    lateinit var grantApplicationRepository: GrantApplicationRepository

    @MockBean
    lateinit var dataItemResponseRepository: DataItemResponseRepository

    companion object {
        private val grantApplicationDAO = ObjectsDAO.grantApplicationWithReviewer1DAO
        private val dataItemResponseDAO = ObjectsDAO.dataItemResponse1DAO
    }

    /*Get Grant Application By Id Test*/
    @Test
    fun `Test GET Grant Application By Id `() {
        Mockito.`when`(grantApplicationRepository.findById(grantApplicationDAO.id))
                .thenReturn(Optional.of(grantApplicationDAO))
        Assert.assertEquals(grantApplicationsServices.getGrantApplicationById(grantApplicationDAO.id), grantApplicationDAO)
    }

    @Test(expected = NotFoundException::class)
    fun `Test GET Grant Application By Id (Not Found Exception)`() {
        Mockito.`when`(grantApplicationRepository.findById(grantApplicationDAO.id))
                .thenReturn(Optional.empty())
        grantApplicationsServices.getGrantApplicationById(grantApplicationDAO.id)
    }

    /*Get All Grant Applications Test*/
    @Test
    fun `Test ALL Grant Applications (Non Empty List)`() {
        Mockito.`when`(grantApplicationRepository.findAll())
                .thenReturn(listOf(grantApplicationDAO))
        Assert.assertEquals(grantApplicationsServices.getAllGrantApplications(), listOf(grantApplicationDAO))
    }

    /*Create Grant Applications Test*/
    @Test
    fun `Test Create Grant Applications (Success)`() {
        val aux = grantApplicationDAO
        aux.fieldsResponses.add(dataItemResponseDAO)
        Mockito.`when`(grantApplicationRepository.existsByStudentEqualsAndGrantCallEquals(grantApplicationDAO.student, grantApplicationDAO.grantCall))
                .thenReturn(false)
        Mockito.`when`(dataItemResponseRepository.saveAll(mutableListOf(dataItemResponseDAO)))
                .thenReturn(mutableListOf(dataItemResponseDAO))
        Mockito.`when`(grantApplicationRepository.save(aux))
                .thenReturn(aux)
        grantApplicationsServices.addGrantApplication(mutableListOf(dataItemResponseDAO), grantApplicationDAO)
    }


    @Test(expected = ConflictException::class)
    fun `Test Create Grant Applications (Conflict)`() {
        Mockito.`when`(grantApplicationRepository.existsByStudentEqualsAndGrantCallEquals(grantApplicationDAO.student, grantApplicationDAO.grantCall))
                .thenReturn(true)
        grantApplicationsServices.addGrantApplication(mutableListOf(dataItemResponseDAO), grantApplicationDAO)
    }

    /*Create Grant Applications Test*/
    @Test
    fun `Test Delete Grant Applications (Success)`() {
        Mockito.`when`(grantApplicationRepository.findById(grantApplicationDAO.id))
                .thenReturn(Optional.of(grantApplicationDAO))
        Mockito.doNothing()
                .`when`(grantApplicationRepository).deleteById(grantApplicationDAO.id)
        grantApplicationsServices.deleteGrantApplication(grantApplicationDAO.id)
    }


    /*Get All Grant Application Reviews*/
    @Test(expected = NotFoundException::class)
    fun `Get All Grant Application Reviews (Not Found)`() {
        Mockito.`when`(grantApplicationRepository.findById(grantApplicationDAO.id))
                .thenReturn(Optional.empty())
        grantApplicationsServices.getAllGrantApplicationReviews(grantApplicationDAO.id)
    }


    @Test
    fun `Get All Grant Application Reviews (Success)`() {
        val grantApplicationDAOAccepted = grantApplicationDAO
        grantApplicationDAOAccepted.status = GrantApplicationStatus.ACCEPTED
        Mockito.`when`(grantApplicationRepository.findById(grantApplicationDAO.id))
                .thenReturn(Optional.of(grantApplicationDAOAccepted))
        Assert.assertEquals(grantApplicationsServices.getAllGrantApplicationReviews(grantApplicationDAOAccepted.id), grantApplicationDAOAccepted.reviews)
    }

}

