package fct.ciai.grantsTests.grantCallTest

import fct.ciai.repositories.evaluationPanels.EvaluationPanelRepository
import fct.ciai.repositories.grants.DataItemRepository
import fct.ciai.repositories.grants.GrantCallRepository
import fct.ciai.services.NotFoundException
import fct.ciai.services.grants.grantsCalls.GrantCallsServices
import fct.ciai.utils.ObjectsDAO
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit4.SpringRunner
import java.util.*

@RunWith(SpringRunner::class)
@SpringBootTest
class GrantCallServiceTester {

    @Autowired
    lateinit var grantCallServices : GrantCallsServices

    @MockBean
    lateinit var grantCallRepository : GrantCallRepository

    @MockBean
    lateinit var dataItemsRepository : DataItemRepository

    @MockBean
    lateinit var evaluationPanelRepository : EvaluationPanelRepository

    companion object{
        private val grantCall1DAO = ObjectsDAO.grantCallDAO
        private val dataItems1DAO = ObjectsDAO.dataItem1DAO
        private val evaPanel1DAO = ObjectsDAO.evaluationPanelDAO


    }

    @Test
    fun `Test GET One Grant Call with Mocked Repository (No Error)`() {
        Mockito.`when`(grantCallRepository.findById(grantCall1DAO.id)).thenReturn(Optional.of(grantCall1DAO))
        Assert.assertEquals(grantCallServices.getGrantCallById(grantCall1DAO.id), grantCall1DAO)
    }

    @Test(expected = NotFoundException::class)
    fun `Test GET One Grant Call with Mocked Repository (No Found)`() {
        Mockito.`when`(grantCallRepository.findById(grantCall1DAO.id)).thenReturn(Optional.empty())
        grantCallServices.getGrantCallById(grantCall1DAO.id)
    }

    @Test(expected = NotFoundException::class)
    fun `Test DELETE One Grant Call with Mocked Repository (Not Found)`() {
        Mockito.`when`(grantCallRepository.findById(grantCall1DAO.id)).thenReturn(Optional.empty())
        grantCallServices.deleteGrantCall(grantCall1DAO.id)
    }

    @Test
    fun `Test DELETE One Grant Call with Mocked Repository (No Error)`() {
        Mockito.`when`(grantCallRepository.findById(grantCall1DAO.id)).thenReturn(Optional.of(grantCall1DAO))
        grantCallServices.deleteGrantCall(grantCall1DAO.id)
    }

    @Test
    fun `Test GET Grant Application given Grant Call with Mocked Repository (No Error)`() {
        Mockito.`when`(grantCallRepository.findById(grantCall1DAO.id)).thenReturn(Optional.of(grantCall1DAO))
        Assert.assertEquals(grantCallServices.getAllGrantApplications(grantCall1DAO.id), grantCall1DAO.grantApplications)
    }

    @Test(expected = NotFoundException::class)
    fun `Test GET Grant Application given Grant Call with Mocked Repository (No Found)`() {
        Mockito.`when`(grantCallRepository.findById(grantCall1DAO.id)).thenReturn(Optional.empty())
        grantCallServices.getAllGrantApplications(grantCall1DAO.id)
    }

    @Test
    fun `Test GET Grant Call on status all with Mocked Repository (No Error)`() {
        Mockito.`when`(grantCallRepository.findAll()).thenReturn(listOf(grantCall1DAO))
        Assert.assertEquals(grantCallServices.getAllGrantOnStatus("all"), listOf(grantCall1DAO))
    }

    @Test
    fun `Test Create Grant Call  with Mocked Repository (No Error)`() {
        Mockito.`when`(grantCallRepository.save(grantCall1DAO)).thenReturn(grantCall1DAO)
        Mockito.`when`(evaluationPanelRepository.save(evaPanel1DAO)).thenReturn(evaPanel1DAO)
        Mockito.`when`(dataItemsRepository.saveAll(listOf(dataItems1DAO))).thenReturn(listOf(dataItems1DAO))
        grantCallServices.addGrantCall(grantCall1DAO, mutableListOf(dataItems1DAO), evaPanel1DAO)
    }


}

