package fct.ciai.userTest.studentTest

import fct.ciai.daos.StudentDAO
import fct.ciai.dtos.accounts.users.students.StudentDTO
import fct.ciai.services.ConflictException
import fct.ciai.services.NotFoundException
import fct.ciai.services.institutions.InstitutionsServices
import fct.ciai.services.users.students.StudentsServices
import fct.ciai.utils.ObjectsDAO
import fct.ciai.utils.Utils
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.data.domain.PageRequest
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status


@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class StudentControllerTester {

    @Autowired
    lateinit var mvc: MockMvc

    @MockBean
    lateinit var studentsServices: StudentsServices

    @MockBean
    lateinit var institutionsServices: InstitutionsServices

    companion object {
        const val STUDENTS_URL = "/students"

        private val emptyList = ObjectsDAO.genericEmptyList

        private val institution1DAO = ObjectsDAO.institution1DAO
        private val student1DAO = ObjectsDAO.student1DAO
        private val student2DAO = ObjectsDAO.student2DAO

        private val student1DTO = StudentDTO(student1DAO)
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET Student (Success)`() {
        Mockito.`when`(studentsServices.getStudentById(student1DAO.id)).thenReturn(student1DAO)

        mvc.perform(get("$STUDENTS_URL/${student1DAO.id}"))
                .andExpect(status().isOk)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET Student (Not Found)`() {
        Mockito.`when`(studentsServices.getStudentById(student1DAO.id)).thenThrow(NotFoundException(""))
        mvc.perform(get("$STUDENTS_URL/${student1DAO.id}")).andExpect(status().isNotFound)
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET All Students (EmptyList)`() {
        Mockito.`when`(studentsServices.getAllStudents(PageRequest.of(0, 10))).thenReturn(listOf())

        val result = mvc.perform(get(STUDENTS_URL))
                .andExpect(status().isOk)
                .andReturn()

        val responseList = Utils.toObject<Iterable<StudentDAO>>(result)
        assertEquals(responseList , emptyList)
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET All Students (Non EmptyList)`() {
        Mockito.`when`(studentsServices.getAllStudents(PageRequest.of(0, 10)))
                .thenReturn(listOf(student1DAO, student2DAO))

        mvc.perform(get(STUDENTS_URL))
                .andExpect(status().isOk)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test CREATE Student (Conflict Error)`() {
        Mockito.`when`(institutionsServices.getInstitutionById(student1DAO.institution.id)).thenReturn(institution1DAO)
        Mockito.`when`(studentsServices.addStudent(student1DAO)).thenThrow(ConflictException(""))

        mvc.perform(post(STUDENTS_URL)
                .contentType(APPLICATION_JSON)
                .content(Utils.toJson(student1DTO)))
                .andExpect(status().isConflict)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test CREATE Student (Success)`() {
        Mockito.`when`(studentsServices.addStudent(student1DAO)).thenReturn(student1DAO)
        Mockito.`when`(institutionsServices.getInstitutionById(student1DAO.institution.id)).thenReturn(institution1DAO)

        val result = mvc.perform(post(STUDENTS_URL)
                .contentType(APPLICATION_JSON)
                .content(Utils.toJson(student1DTO)))
                .andExpect(status().isCreated)
                .andReturn()

        val response = Utils.toObject<StudentDTO>(result)
        assertEquals(response, student1DTO)
    }
}
