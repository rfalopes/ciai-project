package fct.ciai.userTest.studentTest

import fct.ciai.repositories.cv.CVRepository
import fct.ciai.repositories.institutions.InstitutionRepository
import fct.ciai.repositories.users.AccountRepository
import fct.ciai.repositories.users.students.StudentRepository
import fct.ciai.services.ConflictException
import fct.ciai.services.NotFoundException
import fct.ciai.services.users.students.StudentsServices
import fct.ciai.utils.ObjectsDAO
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.data.domain.PageRequest
import org.springframework.test.context.junit4.SpringRunner
import java.util.*

@RunWith(SpringRunner::class)
@SpringBootTest
class StudentServiceTester {

    @Autowired
    lateinit var studentsServices : StudentsServices

    @MockBean
    lateinit var studentRepository : StudentRepository

    @MockBean
    lateinit var accountRepository : AccountRepository

    @MockBean
    lateinit var cvRepository : CVRepository

    @MockBean
    lateinit var institutionRepository : InstitutionRepository

    companion object{
        private val student1DAO = ObjectsDAO.student1DAO
        private val student2DAO = ObjectsDAO.student2DAO
        private val grantApplicationDAO = ObjectsDAO.grantApplicationDAO
    }

    @Test
    fun `Test GET One Student with Mocked Repository (No Error)`() {
        Mockito.`when`(studentRepository.findById(1L)).thenReturn(Optional.of(student1DAO))
        assertEquals(studentsServices.getStudentById(1L),student1DAO)
    }

    @Test(expected = NotFoundException::class)
    fun `Test GET One Student with Mocked Repository (Not Found Error)`() {
        Mockito.`when`(studentRepository.findById(1L)).thenReturn(Optional.empty())
        studentsServices.getStudentById(1L)
    }

    @Test
    fun `Test GET All Student with Mocked Repository (No Error)`() {
        val students = listOf(student1DAO, student2DAO)
        Mockito.`when`(studentRepository.findAll())
                .thenReturn(students)
        assertEquals(studentsServices.getAllStudents(), students)
    }

    @Test
    fun `Test CREATE Student with Mocked Repository (No Error)`() {
        Mockito.`when`(accountRepository.existsByUserNameEqualsOrEmailEquals(student1DAO.userName, student1DAO.email))
                .thenReturn(false)
        Mockito.`when`(studentRepository.save(student1DAO))
                .thenReturn(student1DAO)
        Mockito.`when`(cvRepository.save(student1DAO.cv))
                .thenReturn(student1DAO.cv)
        Mockito.`when`(institutionRepository.save(student1DAO.institution))
                .thenReturn(student1DAO.institution)

        studentsServices.addStudent(student1DAO)
    }

    @Test(expected = ConflictException::class)
    fun `Test CREATE Student with Mocked Repository (Conflict Error)`() {
        Mockito.`when`(accountRepository.existsByUserNameEqualsOrEmailEquals(student1DAO.userName, student1DAO.email))
                .thenReturn(true)

        studentsServices.addStudent(student1DAO)
    }


    @Test
    fun `Test DELETE Student with Mocked Repository (No Error)`() {
        Mockito.`when`(studentRepository.findById(student1DAO.id))
                .thenReturn(Optional.of(student1DAO))
        studentsServices.deleteStudent(student1DAO.id)
    }

    @Test(expected = NotFoundException::class)
    fun `Test DELETE Student with Mocked Repository (Not Found Error)`() {
        Mockito.`when`(studentRepository.findById(student1DAO.id))
                .thenReturn(Optional.empty())
        studentsServices.deleteStudent(student1DAO.id)
    }

    @Test
    fun `Test GET Student applications with Mocked Repository`(){
        student1DAO.grantApplications.add(grantApplicationDAO)
        Mockito.`when`(studentRepository.findById(student1DAO.id))
                .thenReturn(Optional.of(student1DAO))

        assertEquals(studentsServices.getStudentApplications(student1DAO.id), student1DAO.grantApplications)

    }

    @Test(expected = NotFoundException::class)
    fun `Test GET Student applications with an nonexistent student with Mocked Repository`(){
        student1DAO.grantApplications.add(grantApplicationDAO)
        Mockito.`when`(studentRepository.findById(student1DAO.id))
                .thenReturn(Optional.empty())
        studentsServices.getStudentApplications(student1DAO.id)

    }

}
