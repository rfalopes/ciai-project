package fct.ciai.userTest.reviewerTest

import fct.ciai.dtos.accounts.users.reviewers.ReviewerDTO
import fct.ciai.repositories.users.reviewers.ReviewerRepository
import fct.ciai.services.institutions.InstitutionsServices
import fct.ciai.services.users.reviewers.ReviewersServices
import fct.ciai.userTest.reviewerTest.ReviewerControllerTester.Companion.REVIEWERS_URL
import fct.ciai.utils.ObjectsDAO
import fct.ciai.utils.Utils
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import java.util.*

@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class ReviewerAuthorizationTester {

    @Autowired
    lateinit var mvc: MockMvc

    @MockBean
    lateinit var reviewersRepository: ReviewerRepository

    @MockBean
    lateinit var reviewersService: ReviewersServices

    @MockBean
    lateinit var institutionsServices: InstitutionsServices

    companion object {
        private val reviewer1DAO = ObjectsDAO.reviewer1DAO
        private val reviewer2DAO = ObjectsDAO.reviewer2DAO

        private val reviewer2DTO = ReviewerDTO(reviewer2DAO)
    }

    /*Get Reviewers By Id Test*/
    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET Reviewer By Id (ADMIN - Authorized)`() {
        Mockito.`when`(reviewersService.getReviewerById(reviewer1DAO.id)).thenReturn(reviewer1DAO)
        mvc.perform(MockMvcRequestBuilders.get("$REVIEWERS_URL/${reviewer1DAO.id}"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "sponsor1", password = "sponsor1", authorities = ["ROLE_SPONSOR"])
    fun `Test GET Reviewer By Id (SPONSOR - Authorized)`() {
        Mockito.`when`(reviewersService.getReviewerById(reviewer1DAO.id)).thenReturn(reviewer1DAO)
        mvc.perform(MockMvcRequestBuilders.get("$REVIEWERS_URL/${reviewer1DAO.id}"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "student1", password = "student1", authorities = ["ROLE_STUDENT"])
    fun `Test GET Reviewer By Id (STUDENT - Not Authorized)`() {
        Mockito.`when`(reviewersService.getReviewerById(reviewer1DAO.id)).thenReturn(reviewer1DAO)
        mvc.perform(MockMvcRequestBuilders.get("$REVIEWERS_URL/${reviewer1DAO.id}"))
                .andExpect(MockMvcResultMatchers.status().isForbidden)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "reviewer2", password = "reviewer2", authorities = ["ROLE_REVIEWER"])
    fun `Test GET Reviewer By Id (REVIEWER - Not Authorized)`() {
        Mockito.`when`(reviewersRepository.findById(reviewer2DAO.id)).thenReturn(Optional.of(reviewer2DAO))
        Mockito.`when`(reviewersService.getReviewerById(reviewer1DAO.id)).thenReturn(reviewer1DAO)
        mvc.perform(MockMvcRequestBuilders.get("$REVIEWERS_URL/${reviewer1DAO.id}"))
                .andExpect(MockMvcResultMatchers.status().isForbidden)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "reviewer1", password = "reviewer1", authorities = ["ROLE_REVIEWER"])
    fun `Test GET Reviewer By Id (REVIEWER - Authorized)`() {
        // Need in ReviewersSecurityService call
        Mockito.`when`(reviewersRepository.findById(reviewer1DAO.id)).thenReturn(Optional.of(reviewer1DAO))
        Mockito.`when`(reviewersService.getReviewerById(reviewer1DAO.id)).thenReturn(reviewer1DAO)
        mvc.perform(MockMvcRequestBuilders.get("$REVIEWERS_URL/${reviewer1DAO.id}"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andReturn()
    }

    /*Get ALL Reviewers Test*/
    @Test
    fun `Test GET All Reviewers (ADMIN - not Authenticated)`() {
        mvc.perform(MockMvcRequestBuilders.get(REVIEWERS_URL))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test GET All Reviewers (ADMIN - Authorized)`() {
        mvc.perform(MockMvcRequestBuilders.get(REVIEWERS_URL))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "sponsor1", password = "sponsor1", authorities = ["ROLE_SPONSOR"])
    fun `Test GET All Reviewers (SPONSOR - Authorized)`() {
        mvc.perform(MockMvcRequestBuilders.get(REVIEWERS_URL))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "student1", password = "student1", authorities = ["ROLE_STUDENT"])
    fun `Test GET All Reviewers (STUDENT - Not Authorized)`() {
        mvc.perform(MockMvcRequestBuilders.get(REVIEWERS_URL))
                .andExpect(MockMvcResultMatchers.status().isForbidden)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "reviewer2", password = "reviewer2", authorities = ["ROLE_REVIEWER"])
    fun `Test GET All Reviewers (REVIEWER - Not Authorized)`() {
        mvc.perform(MockMvcRequestBuilders.get(REVIEWERS_URL))
                .andExpect(MockMvcResultMatchers.status().isForbidden)
                .andReturn()
    }

    /*CREATE Reviewer Test*/
    @Test
    fun `Test CREATE Reviewer (ADMIN - Not Authenticated)`() {
        mvc.perform(MockMvcRequestBuilders.post(REVIEWERS_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(Utils.toJson(reviewer2DTO)))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test CREATE Reviewer (ADMIN - Authorized)`() {
        Mockito.`when`(institutionsServices.getInstitutionById(reviewer2DAO.institution.id)).thenReturn(reviewer2DAO.institution)
        Mockito.doNothing().`when`(reviewersService).addReviewer(reviewer2DAO)

        mvc.perform(MockMvcRequestBuilders.post(REVIEWERS_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(Utils.toJson(reviewer2DTO)))
                .andExpect(MockMvcResultMatchers.status().isCreated)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "sponsor1", password = "sponsor1", authorities = ["ROLE_SPONSOR"])
    fun `Test CREATE Reviewer (SPONSOR - Not Authorized)`() {
        mvc.perform(MockMvcRequestBuilders.post(REVIEWERS_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(Utils.toJson(reviewer2DTO)))
                .andExpect(MockMvcResultMatchers.status().isForbidden)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "student1", password = "student1", authorities = ["ROLE_STUDENT"])
    fun `Test CREATE Reviewer (STUDENT - Not Authorized)`() {
        mvc.perform(MockMvcRequestBuilders.post(REVIEWERS_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(Utils.toJson(reviewer2DTO)))
                .andExpect(MockMvcResultMatchers.status().isForbidden)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "reviewer1", password = "reviewer1", authorities = ["ROLE_REVIEWER"])
    fun `Test CREATE Reviewer (REVIEWER - Not Authorized)`() {
        mvc.perform(MockMvcRequestBuilders.post(REVIEWERS_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(Utils.toJson(reviewer2DTO)))
                .andExpect(MockMvcResultMatchers.status().isForbidden)
                .andReturn()
    }

    /*DELETE Reviewer Test*/
    @Test
    fun `Test DELETE Reviewer (ADMIN - Not Authenticated)`() {
        mvc.perform(MockMvcRequestBuilders.delete("$REVIEWERS_URL/${reviewer1DAO.id}"))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test DELETE Reviewer (ADMIN - Authorized)`() {
        Mockito.doNothing().`when`(reviewersService).deleteReviewer(reviewer1DAO.id)

        mvc.perform(MockMvcRequestBuilders.delete("$REVIEWERS_URL/${reviewer1DAO.id}"))
                .andExpect(MockMvcResultMatchers.status().isNoContent)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "sponsor1", password = "sponsor1", authorities = ["ROLE_SPONSOR"])
    fun `Test DELETE Reviewer (SPONSOR - Not Authorized)`() {
        mvc.perform(MockMvcRequestBuilders.delete("$REVIEWERS_URL/${reviewer1DAO.id}"))
                .andExpect(MockMvcResultMatchers.status().isForbidden)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "student1", password = "student1", authorities = ["ROLE_STUDENT"])
    fun `Test DELETE Reviewer (STUDENT - Not Authorized)`() {
        mvc.perform(MockMvcRequestBuilders.delete("$REVIEWERS_URL/${reviewer1DAO.id}"))
                .andExpect(MockMvcResultMatchers.status().isForbidden)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "reviewer2", password = "reviewer2", authorities = ["ROLE_REVIEWER"])
    fun `Test DELETE Reviewer (REVIEWER - Not Authorized)`() {
        mvc.perform(MockMvcRequestBuilders.delete("$REVIEWERS_URL/${reviewer1DAO.id}"))
                .andExpect(MockMvcResultMatchers.status().isForbidden)
                .andReturn()
    }

    /*Get Reviewers EvaluationPanels Test*/
    @Test
    @WithMockUser(username = "admin", password = "admin", authorities = ["ROLE_ADMIN"])
    fun `Test Get Reviewers EvaluationPanels (ADMIN - Authorized)`() {
        Mockito.`when`(reviewersService.getAllReviewerEvaluationPanels(reviewer1DAO.id)).thenReturn(listOf())
        mvc.perform(MockMvcRequestBuilders.get("$REVIEWERS_URL/${reviewer1DAO.id}/evaluations_panels"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "sponsor1", password = "sponsor1", authorities = ["ROLE_SPONSOR"])
    fun `Test Get Reviewers EvaluationPanels (SPONSOR - Authorized)`() {
        Mockito.`when`(reviewersService.getAllReviewerEvaluationPanels(reviewer1DAO.id)).thenReturn(listOf())
        mvc.perform(MockMvcRequestBuilders.get("$REVIEWERS_URL/${reviewer1DAO.id}/evaluations_panels"))
                .andExpect(MockMvcResultMatchers.status().isForbidden)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "student1", password = "student1", authorities = ["ROLE_STUDENT"])
    fun `Test Get Reviewers EvaluationPanels (STUDENT - Not Authorized)`() {
        Mockito.`when`(reviewersService.getAllReviewerEvaluationPanels(reviewer1DAO.id)).thenReturn(listOf())
        mvc.perform(MockMvcRequestBuilders.get("$REVIEWERS_URL/${reviewer1DAO.id}/evaluations_panels"))
                .andExpect(MockMvcResultMatchers.status().isForbidden)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "reviewer2", password = "reviewer2", authorities = ["ROLE_REVIEWER"])
    fun `Test Get Reviewers EvaluationPanels (REVIEWER - Not Authorized)`() {
        Mockito.`when`(reviewersRepository.findById(reviewer2DAO.id)).thenReturn(Optional.of(reviewer2DAO))
        Mockito.`when`(reviewersService.getAllReviewerEvaluationPanels(reviewer1DAO.id)).thenReturn(listOf())
        mvc.perform(MockMvcRequestBuilders.get("$REVIEWERS_URL/${reviewer1DAO.id}/evaluations_panels"))
                .andExpect(MockMvcResultMatchers.status().isForbidden)
                .andReturn()
    }

    @Test
    @WithMockUser(username = "reviewer1", password = "reviewer1", authorities = ["ROLE_REVIEWER"])
    fun `Test Get Reviewers EvaluationPanels (REVIEWER - Authorized)`() {
        // Need in ReviewersSecurityService call
        Mockito.`when`(reviewersRepository.findById(reviewer1DAO.id)).thenReturn(Optional.of(reviewer1DAO))
        Mockito.`when`(reviewersService.getAllReviewerEvaluationPanels(reviewer1DAO.id)).thenReturn(listOf())
        mvc.perform(MockMvcRequestBuilders.get("$REVIEWERS_URL/${reviewer1DAO.id}/evaluations_panels"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andReturn()
    }
}
