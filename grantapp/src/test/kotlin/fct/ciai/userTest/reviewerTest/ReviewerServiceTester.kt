package fct.ciai.userTest.reviewerTest

import fct.ciai.repositories.institutions.InstitutionRepository
import fct.ciai.repositories.users.AccountRepository
import fct.ciai.repositories.users.reviewers.ReviewerRepository
import fct.ciai.services.ConflictException
import fct.ciai.services.NotFoundException
import fct.ciai.services.users.reviewers.ReviewersServices
import fct.ciai.utils.ObjectsDAO
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.data.domain.PageRequest
import org.springframework.test.context.junit4.SpringRunner
import java.util.*

@RunWith(SpringRunner::class)
@SpringBootTest
class ReviewerServiceTester {


    @Autowired
    lateinit var reviewerServices: ReviewersServices

    @MockBean
    lateinit var reviewerRepository : ReviewerRepository

    @MockBean
    lateinit var accountRepository : AccountRepository

    @MockBean
    lateinit var institutionRepository : InstitutionRepository


    companion object {
        private val emptyList = ObjectsDAO.genericEmptyList
        private val reviewer1DAO = ObjectsDAO.reviewer1DAO
        private val reviewer2DAO = ObjectsDAO.reviewer2DAO
    }

    @Test
    fun `Test GET One Reviewer with Mocked Repository (No Error)`() {
        Mockito.`when`(reviewerRepository.findById(1L)).thenReturn(Optional.of(reviewer1DAO))
        assertEquals(reviewerServices.getReviewerById(1L), reviewer1DAO)
    }

    @Test(expected = NotFoundException::class)
    fun `Test GET One Reviewer with Mocked Repository (Not Found Error)`() {
        Mockito.`when`(reviewerRepository.findById(1L)).thenReturn(Optional.empty())
        reviewerServices.getReviewerById(1L)
    }

    @Test
    fun `Test GET All Reviewer with Mocked Repository (No Error)`() {
        val students = listOf(reviewer1DAO, reviewer2DAO)
        Mockito.`when`(reviewerRepository.findAll())
                .thenReturn(students)
        assertEquals(reviewerServices.getAllReviewers(), students)
    }

    @Test
    fun `Test CREATE Reviewer with Mocked Repository (No Error)`() {
        Mockito.`when`(accountRepository
                .existsByUserNameEqualsOrEmailEquals(reviewer1DAO.userName, reviewer1DAO.email))
                .thenReturn(false)
        Mockito.`when`(reviewerRepository.save(reviewer1DAO))
                .thenReturn(reviewer1DAO)
        Mockito.`when`(institutionRepository.save(reviewer1DAO.institution))
                .thenReturn(reviewer1DAO.institution)

        reviewerServices.addReviewer(reviewer1DAO)
    }

    @Test(expected = ConflictException::class)
    fun `Test CREATE Reviewer with Mocked Repository (Conflict Error)`() {
        Mockito.`when`(accountRepository
                .existsByUserNameEqualsOrEmailEquals(reviewer1DAO.userName, reviewer1DAO.email))
                .thenReturn(true)

        reviewerServices.addReviewer(reviewer1DAO)
    }


    @Test
    fun `Test DELETE Reviewer with Mocked Repository (No Error)`() {
        Mockito.`when`(reviewerRepository.findById(reviewer1DAO.id))
                .thenReturn(Optional.of(reviewer1DAO))
        reviewerServices.deleteReviewer(reviewer1DAO.id)
    }

    @Test(expected = NotFoundException::class)
    fun `Test DELETE Reviewer with Mocked Repository (Not Found Error)`() {
        Mockito.`when`(reviewerRepository.findById(reviewer1DAO.id))
                .thenReturn(Optional.empty())
        reviewerServices.deleteReviewer(reviewer1DAO.id)
    }

    @Test
    fun `Test GET All Reviewer Evaluation Panels (Success)`() {
        Mockito.`when`(reviewerRepository.findById(reviewer1DAO.id))
                .thenReturn(Optional.of(reviewer1DAO))
        val reviewer = reviewerServices.getAllReviewerEvaluationPanels(reviewer1DAO.id)
        assertEquals(reviewer, emptyList)
    }

    @Test(expected = NotFoundException::class)
    fun `Test GET All Reviewer Evaluation Panels (Not Found)`() {
        Mockito.`when`(reviewerRepository.findById(reviewer1DAO.id))
                .thenThrow(NotFoundException(""))
        reviewerServices.getAllReviewerEvaluationPanels(reviewer1DAO.id)
    }

}
