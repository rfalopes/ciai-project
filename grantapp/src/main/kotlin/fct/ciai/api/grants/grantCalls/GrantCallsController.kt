package fct.ciai.api.grants.grantCalls

import fct.ciai.daos.*
import fct.ciai.dtos.grantApplications.GrantApplicationDTO
import fct.ciai.dtos.GrantCallDTO
import fct.ciai.dtos.GrantCallSponsorDTO
import fct.ciai.dtos.grantApplications.GrantApplicationDetailsDTO
import fct.ciai.enums.GrantApplicationStatus
import fct.ciai.services.grants.grantsCalls.GrantCallsServices
import fct.ciai.services.sponsors.SponsorsServices
import fct.ciai.services.users.reviewers.ReviewersServices
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/grant_calls")
class GrantCallsController(
        @Autowired val grantCallsService: GrantCallsServices,
        @Autowired val sponsorsServices: SponsorsServices,
        @Autowired val reviewersServices: ReviewersServices
) : GrantCallsAPI {

    private val logger = LoggerFactory.getLogger(javaClass)

    //TODO: User Stories: 2
    override fun getGrantCallById(id: Long): GrantCallDTO {
        logger.info("GET Grant Call by ID")
        return GrantCallDTO(grantCallsService.getGrantCallById(id))
    }

    override fun createGrantCall(grantCall: GrantCallDTO) {
        logger.info("CREATE Grant Call")
        val sponsorDAO = sponsorsServices.getSponsorById(grantCall.sponsorId)
        val evaluationPanelDTO = grantCall.evaluationPanel
        val reviewersDAO = reviewersServices.getReviewersById(evaluationPanelDTO.reviewersIDs)
        val panelChairDAO = reviewersServices.getReviewerById(evaluationPanelDTO.panelChair)
        val evaluationPanelDAO = EvaluationPanelDAO(evaluationPanelDTO.id, panelChairDAO, reviewersDAO)

        val grantCallDAO = GrantCallDAO(grantCall.id, grantCall.title, grantCall.description, grantCall.startDate,
                grantCall.expireDate, grantCall.funding, evaluationPanelDAO, mutableListOf(), sponsorDAO)

        val dataItemsDAO = grantCall.dataItems.map {
            DataItemDAO(it.id, it.type, it.header, it.subTitle, it.mandatory, grantCallDAO)
        }.toMutableList()

        /*Create Grant Call*/
        grantCallsService.addGrantCall(grantCallDAO, dataItemsDAO, evaluationPanelDAO)
    }

    override fun deleteGrantCall(id: Long) {
        logger.info("DELETE Grant Call by ID")
        grantCallsService.deleteGrantCall(id)
    }

    override fun getGrantCallApplications(id: Long): List<GrantApplicationDTO> {
        logger.info("GET Grant Call Applications by ID")
        return grantCallsService.getAllGrantApplications(id).map { GrantApplicationDTO(it) }
    }



    //TODO: User Stories: 1
    override fun getGrantCallsOnStatus(page: Int, size: Int, status: String): List<GrantCallDTO> {
        return grantCallsService.getAllGrantOnStatus(status).map { GrantCallDTO(it) }
    }

    override fun getClosedGrantCalls(page: Int, size: Int): List<GrantCallDTO> {
        return grantCallsService.getAllGrantOnStatus("close").map { GrantCallDTO(it) }
    }

    override fun getAcceptedGrantCallApplications(id: Long): List<GrantApplicationDetailsDTO> {
        val grantApplications = grantCallsService.getAcceptedGrantCallApplications(id)
        //REFECTORY: More efficient - Done in phase 4
        return grantApplications.map { GrantApplicationDetailsDTO(it, it.grantCall.title, it.student.name) }
    }

    override fun getGrantCallByIdComplete(id: Long): GrantCallSponsorDTO {
        return GrantCallSponsorDTO(grantCallsService.getGrantCallById(id))
    }

    override fun getGrantCallsOnStatusComplete(status: String): List<GrantCallSponsorDTO> {
        return grantCallsService.getAllGrantOnStatus(status).map { GrantCallSponsorDTO(it) }
    }

    override fun getGrantCallApplicationsByStatus(id: Long, status: GrantApplicationStatus): List<GrantApplicationDetailsDTO> {
        val grantApplications = grantCallsService.getGrantCallApplicationsByStatus(id, status)
        return grantApplications.map { GrantApplicationDetailsDTO(it, it.grantCall.title, it.student.name) }
    }

}
