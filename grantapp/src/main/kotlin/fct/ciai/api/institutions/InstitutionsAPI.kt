package fct.ciai.api.institutions

import fct.ciai.authorization.institutions.AllowForCreateInstitution
import fct.ciai.authorization.institutions.AllowForDeleteInstitution
import fct.ciai.authorization.institutions.AllowForGetAllInstitutions
import fct.ciai.authorization.institutions.AllowForGetInstitution
import fct.ciai.dtos.InstitutionDTO
import io.swagger.annotations.*
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@Api(value = "Grant Application Management System",
        description= "Manage Operations of Institution in the Grant Application System")
@RequestMapping("/institutions")
interface InstitutionsAPI {

    @ApiOperation(value = "View a list of registered Institutions.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully returned all the registered institutions."),
        ApiResponse(code = 401, message = "Unauthorized institutions get."),
        ApiResponse(code = 403, message = "Forbidden institutions get."),
        ApiResponse(code = 404, message = "Institutions not found.")
    ])
    @GetMapping("")
    @ResponseStatus(HttpStatus.OK)
    @AllowForGetAllInstitutions
    fun getAllInstitutions(
            @RequestParam(value = "page", defaultValue = "0") page: Int,
            @RequestParam(value = "size", defaultValue = "10") size: Int
    ): List<InstitutionDTO>

    //-------------------------------------------------------------------

    @ApiOperation(value = "View a specific Institution.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully returned Institution."),
        ApiResponse(code = 401, message = "Unauthorized institutions get."),
        ApiResponse(code = 403, message = "Forbidden institutions get."),
        ApiResponse(code = 404, message = "Institution with the given id is not registered in the system.")
    ])
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @AllowForGetInstitution
    fun getInstitutionById(@PathVariable id : Long): InstitutionDTO?

    //-------------------------------------------------------------------

    @ApiOperation(value = "Register a new Institution in the system.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully registered the Institution in the system."),
        ApiResponse(code = 201, message = "Successfully registered a new institution in the system."),
        ApiResponse(code = 204, message = "Institution with the given id already exists in the system."),
        ApiResponse(code = 401, message = "Unauthorized institutions create."),
        ApiResponse(code = 403, message = "Forbidden institutions create."),
        ApiResponse(code = 404, message = "Resource not found."),
        ApiResponse(code = 409, message = "The Institution is already registered in the system.")
    ])
    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    @AllowForCreateInstitution
    fun createInstitution(@RequestBody institution: InstitutionDTO)

    //-------------------------------------------------------------------

    @ApiOperation(value = "Delete a Institution from the system.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully deleted the Institution from the system."),
        ApiResponse(code = 204, message = "Successfully deleted the Institution from the system. No content."),
        ApiResponse(code = 401, message = "Unauthorized institutions delete."),
        ApiResponse(code = 403, message = "Forbidden institutions delete."),
        ApiResponse(code = 404, message = "Institution with the given id is not registered in the system.")
    ])
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @AllowForDeleteInstitution
    fun deleteInstitution(@PathVariable id:Long)

    //-------------------------------------------------------------------

}
