package fct.ciai.api.login

import fct.ciai.dtos.LoginFormDTO
import fct.ciai.dtos.LoginResponseDTO
import fct.ciai.services.users.AccountsService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/login")
class LoginController(
        @Autowired val accountsService: AccountsService
) : LoginAPI {

    private val logger = LoggerFactory.getLogger(javaClass)

    override fun login(loginFormDTO: LoginFormDTO): LoginResponseDTO {
        logger.info("Login")
        return LoginResponseDTO(accountsService.login(loginFormDTO.username, loginFormDTO.password))
    }

}
