package fct.ciai.api.login

import fct.ciai.dtos.LoginFormDTO
import fct.ciai.dtos.LoginResponseDTO
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@Api(value = "Grant Application Management System",
        description= "Manage Login Operation in the Grant Application System")
@RequestMapping("/login")
interface LoginAPI {

    @ApiOperation(value = "Login Account.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully login."),
        ApiResponse(code = 403, message = "Forbidden CV get."),
    ])
    @PostMapping("")
    @ResponseStatus(HttpStatus.OK)
    fun login(@RequestBody loginFormDTO: LoginFormDTO): LoginResponseDTO

}
