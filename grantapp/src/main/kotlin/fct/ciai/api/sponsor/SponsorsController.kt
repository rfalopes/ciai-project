package fct.ciai.api.sponsor

import fct.ciai.daos.SponsorDAO
import fct.ciai.dtos.GrantCallDTO
import fct.ciai.dtos.accounts.sponsors.SponsorDTO
import fct.ciai.dtos.accounts.sponsors.SponsorWithoutPasswordDTO
import fct.ciai.services.sponsors.SponsorsServices
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/sponsors")
class SponsorsController(
        @Autowired val sponsorsServices: SponsorsServices
) : SponsorsAPI {

    private val logger = LoggerFactory.getLogger(javaClass)

    override fun getAllSponsors(page: Int, size: Int): List<SponsorWithoutPasswordDTO> {
        logger.info("GET ALL Sponsors")
        return sponsorsServices.getAllSponsors(PageRequest.of(page, size)).map { SponsorWithoutPasswordDTO(it) }
    }

    override fun getSponsorById(id: Long): SponsorWithoutPasswordDTO {
        logger.info("GET Sponsor by ID")
        return SponsorWithoutPasswordDTO(sponsorsServices.getSponsorById(id))
    }

    override fun createSponsor(sponsor: SponsorDTO) {
        logger.info("CREATE Sponsor")
        val sponsorDAO = SponsorDAO(
                sponsor.id,
                sponsor.userName,
                sponsor.name,
                sponsor.password,
                sponsor.email,
                sponsor.phoneNumber)
        sponsorsServices.addSponsor(sponsorDAO)
    }

    override fun deleteSponsor(id: Long) {
        logger.info("DELETE Sponsor")
        sponsorsServices.deleteSponsor(id)
    }

    override fun getSponsorGrantCalls(id: Long): List<GrantCallDTO> {
        logger.info("GET Sponsor Grant Calls")
        return sponsorsServices.getSponsorById(id).grantCalls.map { GrantCallDTO(it) }
    }
}
