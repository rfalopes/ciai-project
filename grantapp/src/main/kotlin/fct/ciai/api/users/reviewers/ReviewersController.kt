package fct.ciai.api.users.reviewers

import fct.ciai.daos.ReviewerDAO
import fct.ciai.dtos.EvaluationPanelDTO
import fct.ciai.dtos.GrantCallDTO
import fct.ciai.dtos.accounts.users.reviewers.ReviewerDTO
import fct.ciai.dtos.accounts.users.reviewers.ReviewerWithoutPasswordDTO
import fct.ciai.services.institutions.InstitutionsServices
import fct.ciai.services.users.reviewers.ReviewersServices
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/reviewers")
class ReviewersController(
        @Autowired val reviewersService: ReviewersServices,
        @Autowired val institutionServices: InstitutionsServices
) : ReviewersAPI {

    private val logger = LoggerFactory.getLogger(javaClass)

    override fun getAllReviewers(page: Int, size: Int): List<ReviewerWithoutPasswordDTO> {
        logger.info("GET ALL Reviewers")
        return reviewersService.getAllReviewers(PageRequest.of(page, size)).map { ReviewerWithoutPasswordDTO(it) }
    }

    override fun getReviewerById(id: Long): ReviewerWithoutPasswordDTO {
        logger.info("GET Reviewer by ID")
        return ReviewerWithoutPasswordDTO(reviewersService.getReviewerById(id))
    }

    override fun createReviewer(reviewer: ReviewerDTO) {
        logger.info("CREATE Reviewer")
        val institutionDAO = institutionServices.getInstitutionById(reviewer.institution)

        val reviewerDAO = ReviewerDAO(reviewer.id,
                reviewer.userName,
                reviewer.name,
                reviewer.password,
                reviewer.email,
                reviewer.birthDate,
                institutionDAO)
        reviewersService.addReviewer(reviewerDAO)
    }

    override fun deleteReviewer(id: Long) {
        logger.info("DELETE Reviewer")
        reviewersService.deleteReviewer(id)
    }

    override fun getAllReviewerEvaluationPanels(id: Long): List<EvaluationPanelDTO> {
        logger.info("GET ALL Reviewer Evaluation Panels by ID")
        return reviewersService.getAllReviewerEvaluationPanels(id).map { EvaluationPanelDTO(it) }
    }

    //TODO: User Stories: 10
    override fun getAllReviewerEvaluationPanelsChairedBy(id: Long): List<EvaluationPanelDTO> {
        logger.info("GET ALL Reviewer Evaluation Panels by ID")
        return reviewersService.getAllReviewerEvaluationPanelsChairedBy(id).map { EvaluationPanelDTO(it) }
    }

    override fun getAllGrantCallsToEvaluation(id: Long): List<GrantCallDTO> {
        logger.info("GET ALL Grant Calls to Evaluate by Reviewer ID")
        return reviewersService.getAllGrantCallsToEvaluation(id).map { GrantCallDTO(it) }
    }

    override fun getAllChairedGrantCalls(id: Long): List<GrantCallDTO> {
        logger.info("GET ALL Chaired Grant Calls by Reviewer ID")
        return reviewersService.getAllChairedGrantCalls(id).map { GrantCallDTO(it) }
    }

    override fun checkIfReviewerIsChair(grantCalID: Long, reviewerID: Long) {
        return reviewersService.checkIfReviewerIsChair(grantCalID, reviewerID)
    }
}

