package fct.ciai.api.users.students

import fct.ciai.daos.GrantCallDAO
import fct.ciai.daos.StudentDAO
import fct.ciai.dtos.GrantCallDTO
import fct.ciai.dtos.grantApplications.GrantApplicationDetailsDTO
import fct.ciai.dtos.accounts.users.students.StudentDTO
import fct.ciai.dtos.accounts.users.students.StudentWithoutPasswordDTO
import fct.ciai.dtos.grantApplications.GrantApplicationForListingDTO
import fct.ciai.enums.GrantApplicationStatus
import fct.ciai.services.institutions.InstitutionsServices
import fct.ciai.services.users.students.StudentsServices
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping("/students")
class StudentsController(
        @Autowired val studentsService: StudentsServices,
        @Autowired val institutionsServices: InstitutionsServices
) : StudentsAPI {

    private val logger = LoggerFactory.getLogger(javaClass)

    override fun getAllStudents(page: Int, size: Int): List<StudentWithoutPasswordDTO> {
        logger.info("GET ALL Students")
        return studentsService.getAllStudents(PageRequest.of(page, size)).map { StudentWithoutPasswordDTO(it) }
    }

    override fun getStudentById(userID: Long): StudentWithoutPasswordDTO {
        logger.info("GET Student by ID")
        return StudentWithoutPasswordDTO(studentsService.getStudentById(userID))
    }

    override fun createStudent(student: StudentDTO): StudentWithoutPasswordDTO {
        logger.info("CREATE Student")
        val institutionDAO = institutionsServices.getInstitutionById(student.institution)
        val studentDAO = StudentDAO(student.id, student.userName, student.name, student.password, student.email,
                student.birthDate, institutionDAO, student.average)
        return StudentWithoutPasswordDTO(studentsService.addStudent(studentDAO))
    }

    override fun deleteStudent(userID: Long) {
        logger.info("DELETE Student")
        return studentsService.deleteStudent(userID)
    }

    /*Updated for front-end - By status*/
    override fun getStudentGrantApplications(userID: Long, status: GrantApplicationStatus?): List<GrantApplicationForListingDTO> {
        logger.info("GET Student Grant Applications")
        //REFECTORY: More efficient - Done in phase 4
        return studentsService.getStudentApplicationsWithGrantCallName(userID, status).map { GrantApplicationForListingDTO(it, it.grantCall.title) }
    }

    override fun getOpenAndNotCreatedGrantCalls(studentID: Long): List<GrantCallDTO> {
        logger.info("GET Student Grant Calls without any submitted or created application")
        return studentsService.getOpenAndNotCreatedGrantCalls(studentID).map { GrantCallDTO(it) }
    }
}
