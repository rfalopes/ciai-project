package fct.ciai.api.users.reviewers

import fct.ciai.authorization.user.reviewers.*
import fct.ciai.dtos.EvaluationPanelDTO
import fct.ciai.dtos.GrantCallDTO
import fct.ciai.dtos.accounts.users.reviewers.ReviewerDTO
import fct.ciai.dtos.accounts.users.reviewers.ReviewerWithoutPasswordDTO
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@Api(value = "Grant Application Management System",
        description = "Manage Operations of Reviewers in the Grant Application System")
@RequestMapping("/reviewers")
interface ReviewersAPI {

    @ApiOperation(value = "View a list of registered reviewers.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully returned all the registered reviewers."),
        ApiResponse(code = 401, message = "Unauthorized reviewers get."),
        ApiResponse(code = 403, message = "Forbidden reviewers get."),
        ApiResponse(code = 404, message = "The reviewers resource does not exit in the system.")
    ])
    @GetMapping("")
    @ResponseStatus(HttpStatus.OK)
    @AllowForGetAllReviewers
    fun getAllReviewers(
            @RequestParam("page", defaultValue = "0") page: Int,
            @RequestParam("size", defaultValue = "10") size: Int
    ): List<ReviewerWithoutPasswordDTO>

    //-------------------------------------------------------------------

    @ApiOperation(value = "Get the details of a single reviewer.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully returned the reviewer with the specified id."),
        ApiResponse(code = 401, message = "Unauthorized reviewer get."),
        ApiResponse(code = 403, message = "Forbidden reviewer get."),
        ApiResponse(code = 404, message = "Reviewer with the given id is not registered in the system.")
    ])
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @AllowForGetReviewer
    fun getReviewerById(@PathVariable id: Long): ReviewerWithoutPasswordDTO

    //-------------------------------------------------------------------

    @ApiOperation(value = "Register a new reviewer in the system.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully registered the reviewer in the system."),
        ApiResponse(code = 201, message = "Successfully registered the reviewer in the system."),
        ApiResponse(code = 401, message = "Unauthorized reviewer create."),
        ApiResponse(code = 403, message = "Forbidden reviewer create."),
        ApiResponse(code = 404, message = "The reviewers resource does not exit in the system."),
        ApiResponse(code = 409, message = "The reviewer is already registered in the system.")
    ])
    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    @AllowForCreateReviewer
    fun createReviewer(@RequestBody reviewer: ReviewerDTO)

    //-------------------------------------------------------------------

    @ApiOperation(value = "Delete a  reviewer from the system.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully deleted the reviewer from the system."),
        ApiResponse(code = 204, message = "Successfully deleted the reviewer from the system. No content."),
        ApiResponse(code = 401, message = "Unauthorized reviewer delete."),
        ApiResponse(code = 403, message = "Forbidden reviewer delete."),
        ApiResponse(code = 404, message = "Reviewer with the given id is not registered in the system.")
    ])
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @AllowForDeleteReviewer
    fun deleteReviewer(@PathVariable id: Long)

    //-------------------------------------------------------------------

    @ApiOperation(value = "Get a list of evaluations panels from specific Reviewer.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully get a list of evaluations panels from specific Reviewer."),
        ApiResponse(code = 401, message = "Unauthorized reviewer's evaluation panels get."),
        ApiResponse(code = 403, message = "Forbidden reviewer's evaluation panels get."),
        ApiResponse(code = 404, message = "Reviewer with the given id is not registered in the system.")
    ])
    @GetMapping("/{id}/evaluations_panels")
    @ResponseStatus(HttpStatus.OK)
    @AllowForGetReviewerEvaluationPanels
    fun getAllReviewerEvaluationPanels(@PathVariable id: Long): List<EvaluationPanelDTO>

    //-------------------------------------------------------------------

    @ApiOperation(value = "Get a list of evaluations panels chaired by specific Reviewer.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully get a list of evaluations panels chaired by specific Reviewer."),
        ApiResponse(code = 401, message = "Unauthorized reviewer's evaluation panels get."),
        ApiResponse(code = 403, message = "Forbidden reviewer's evaluation panels get."),
        ApiResponse(code = 404, message = "Reviewer with the given id is not registered in the system.")
    ])
    @GetMapping("/{id}/evaluations_panels_chaired_by")
    @ResponseStatus(HttpStatus.OK)
    @AllowForGetReviewerEvaluationPanels
    fun getAllReviewerEvaluationPanelsChairedBy(@PathVariable id: Long): List<EvaluationPanelDTO>

    //-------------------------------------------------------------------

    @ApiOperation(value = "Get a list of GrantCalls from specific Reviewer.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully get a list of grant calls associated to a specific Reviewer."),
        ApiResponse(code = 401, message = "Unauthorized reviewer's grant calls get."),
        ApiResponse(code = 403, message = "Forbidden reviewer's grant calls get."),
        ApiResponse(code = 404, message = "Reviewer with the given id is not registered in the system.")
    ])
    @GetMapping("/{id}/grants_to_evaluate")
    @ResponseStatus(HttpStatus.OK)
    @AllowForGetReviewerEvaluationPanels
    fun getAllGrantCallsToEvaluation(@PathVariable id: Long): List<GrantCallDTO>

    //-------------------------------------------------------------------

    @ApiOperation(value = "Get a list of GrantCalls chaired by a given Reviewer.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully get a list of grant calls chaired by a specific Reviewer."),
        ApiResponse(code = 401, message = "Unauthorized reviewer's grant calls get."),
        ApiResponse(code = 403, message = "Forbidden reviewer's grant calls get."),
        ApiResponse(code = 404, message = "Reviewer with the given id is not registered in the system.")
    ])
    @GetMapping("/{id}/chaired_grants")
    @ResponseStatus(HttpStatus.OK)
    @AllowForGetReviewerEvaluationPanels
    fun getAllChairedGrantCalls(@PathVariable id: Long): List<GrantCallDTO>

    //-------------------------------------------------------------------

    @ApiOperation(value = "Check if Reviewer is Chairing a specific grant call.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully returned the ok."),
        ApiResponse(code = 401, message = "Unauthorized request to grant calls."),
        ApiResponse(code = 403, message = "Forbidden request to grant calls."),
        ApiResponse(code = 404, message = "Resource not found.")
    ])
    @GetMapping("/{reviewerID}/grant_calls/{grantCalID}")
    @ResponseStatus(HttpStatus.OK)
    fun checkIfReviewerIsChair(@PathVariable grantCalID : Long, @PathVariable reviewerID : Long)
}
