package fct.ciai.api.reviews

import fct.ciai.authorization.reviews.*
import fct.ciai.dtos.FinalReviewDTO
import fct.ciai.dtos.ReviewDTO
import fct.ciai.dtos.ReviewDetailsDTO
import io.swagger.annotations.*
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@Api(value = "Grant Application Management System",
        description= "Manage Operations of Reviews in the Grant Application System")
@RequestMapping("/reviews")
interface ReviewsAPI {

    @ApiOperation(value = "View a list of Reviews.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully returned all the Reviews."),
        ApiResponse(code = 401, message = "Unauthorized reviews get."),
        ApiResponse(code = 403, message = "Forbidden reviews get."),
        ApiResponse(code = 404, message = "Reviews resource not found in the system.")
    ])
    @GetMapping("")
    @ResponseStatus(HttpStatus.OK)
    @AllowForGetAllReviews
    fun getAllReviews(
            @RequestParam(value = "page", defaultValue = "0") page: Int,
            @RequestParam(value = "size", defaultValue = "10") size: Int
    ): List<ReviewDTO>

    //-------------------------------------------------------------------

    @ApiOperation(value = "View a specific Review.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully returned Review."),
        ApiResponse(code = 401, message = "Unauthorized review get."),
        ApiResponse(code = 403, message = "Forbidden review get."),
        ApiResponse(code = 404, message = "Review with the given id is not registered in the system.")
    ])
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @AllowForGetReviewById
    fun getReviewById(@PathVariable id : Long): ReviewDTO?

    //-------------------------------------------------------------------

    @ApiOperation(value = "Register a new Review in the system.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully registered the Review in the system."),
        ApiResponse(code = 201, message = "Successfully registered the Review in the system. No content."),
        ApiResponse(code = 401, message = "Unauthorized review create."),
        ApiResponse(code = 403, message = "Forbidden review create."),
        ApiResponse(code = 404, message = "Reviews resource not found in the system."),
        ApiResponse(code = 409, message = "The review is already in the system.")
    ])
    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    @AllowForCreateReview
    fun createReview(@RequestBody review: ReviewDTO)

    //-------------------------------------------------------------------

    @ApiOperation(value = "Register the Final Review of Grant Call in the system.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully registered the Final Review in the system."),
        ApiResponse(code = 201, message = "Successfully registered the Final Review in the system. No content."),
        ApiResponse(code = 401, message = "Unauthorized Final Review create."),
        ApiResponse(code = 403, message = "Forbidden Final Review create."),
        ApiResponse(code = 404, message = "Reviews resource not found in the system."),
        ApiResponse(code = 409, message = "The Final Review is already in the system.")
    ])
    @PostMapping("/final_review")
    @ResponseStatus(HttpStatus.CREATED)
    @AllowForCreateFinalReview
    fun createFinalReview(@RequestBody review: FinalReviewDTO)

    //-------------------------------------------------------------------

    @ApiOperation(value = "Update a existing review .")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully updated review."),
        ApiResponse(code = 201, message = "Successfully created review."),
        ApiResponse(code = 401, message = "Unauthorized review update."),
        ApiResponse(code = 403, message = "Forbidden review update."),
        ApiResponse(code = 404, message = "The review does not exit in the system.")
    ])
    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @AllowForUpdateReview
    fun updateReview(@PathVariable id: Long, @RequestBody review: ReviewDTO)

    //-------------------------------------------------------------------

    @ApiOperation(value = "Delete a  Review from the system.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully deleted the Review from the system."),
        ApiResponse(code = 204, message = "Successfully deleted the Review in the system. No content."),
        ApiResponse(code = 401, message = "Unauthorized reviews delete."),
        ApiResponse(code = 403, message = "Forbidden reviews delete."),
        ApiResponse(code = 404, message = "Review with the given id is not registered in the system.")
    ])
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @AllowForDeleteReview
    fun deleteReview(@PathVariable id:Long)

    //-------------------------------------------------------------------

    @ApiOperation(value = "Check if Reviewer has written a Review to a given grantApplication.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully deleted the Review from the system."),
        ApiResponse(code = 204, message = "Successfully deleted the Review in the system. No content."),
        ApiResponse(code = 401, message = "Unauthorized reviews delete."),
        ApiResponse(code = 403, message = "Forbidden reviews delete."),
        ApiResponse(code = 404, message = "Review with the given id is not registered in the system.")
    ])
    @GetMapping("/{reviewerID}/grant_applications/{grantApplicationID}")
    @ResponseStatus(HttpStatus.OK)
    fun checkIfAlreadyReviewed(@PathVariable reviewerID:Long, @PathVariable grantApplicationID:Long)

}
