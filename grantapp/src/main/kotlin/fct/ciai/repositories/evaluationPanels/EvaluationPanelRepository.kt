package fct.ciai.repositories.evaluationPanels

import fct.ciai.daos.EvaluationPanelDAO
import org.springframework.data.repository.CrudRepository

interface EvaluationPanelRepository : CrudRepository<EvaluationPanelDAO, Long> {

}
