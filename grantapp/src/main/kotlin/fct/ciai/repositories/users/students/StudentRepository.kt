package fct.ciai.repositories.users.students

import fct.ciai.daos.GrantCallDAO
import fct.ciai.daos.StudentDAO
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.PagingAndSortingRepository
import java.util.*

interface StudentRepository : PagingAndSortingRepository<StudentDAO, Long> {

    fun findByUserName(userName: String): Optional<StudentDAO>

    @Query("SELECT agc FROM GrantCallDAO agc WHERE agc.status = 'OPEN' and agc not in (SELECT gc FROM GrantCallDAO gc INNER JOIN gc.grantApplications ga INNER JOIN ga.student st WHERE st.id=?1)")
    fun getOpenAndNotCreatedGrantCalls(studentID: Long): List<GrantCallDAO>
}
