package fct.ciai.repositories.review

import fct.ciai.daos.*
import fct.ciai.dtos.accounts.users.reviewers.ReviewerDTO
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.PagingAndSortingRepository
import java.util.*

interface ReviewRepository : PagingAndSortingRepository<ReviewDAO, Long> {
    fun existsByReviewerEqualsAndGrantApplicationEquals(reviewer: ReviewerDAO, grantApplication: GrantApplicationDAO): Boolean

    @Query("SELECT rw from GrantCallDAO gc INNER JOIN gc.grantApplications ga INNER JOIN gc.evaluationPanel ev INNER JOIN ev.panelReviewers rv INNER JOIN ev.panelChair pc INNER JOIN ga.reviews rw WHERE ga.id = ?2 and rw.reviewer.id=?1")
    fun checkIfAlreadyReviewed(reviewerID: Long, grantApplicationID: Long): MutableList<ReviewDAO>
}
