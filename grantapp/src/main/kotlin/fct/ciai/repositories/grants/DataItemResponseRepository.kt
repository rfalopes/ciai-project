package fct.ciai.repositories.grants

import fct.ciai.daos.DataItemResponseDAO
import org.springframework.data.repository.CrudRepository

interface DataItemResponseRepository : CrudRepository<DataItemResponseDAO, Long> {
}