package fct.ciai.repositories.institutions

import fct.ciai.daos.InstitutionDAO
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.PagingAndSortingRepository

interface InstitutionRepository : PagingAndSortingRepository<InstitutionDAO, Long>{
    fun existsByNameEqualsOrEmailEquals(name:String , email: String) : Boolean
}
