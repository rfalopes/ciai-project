package fct.ciai.configurations.security

import fct.ciai.configurations.security.JWTUtil.JWTSecret.KEY
import fct.ciai.configurations.security.JWTUtil.JWTSecret.SUBJECT
import fct.ciai.configurations.security.JWTUtil.JWTSecret.VALIDITY
import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.springframework.security.core.Authentication
import java.util.*
import javax.servlet.http.HttpServletResponse
import kotlin.collections.HashMap

class JWTUtil {

    object JWTSecret {
        private const val passphrase = "b6PC4hGrVILTpqeAwZ7pUWkKkljQLAxcw88BrAOzQyc3EJlLZFulP5ehtyYiOXQQ"
        internal val KEY: String = Base64.getEncoder().encodeToString(passphrase.toByteArray())
        internal const val SUBJECT = "JSON Web Token for CIAI 2019/20"
        internal const val VALIDITY = 1000 * 60 * 10 //10 minutes in milliseconds
    }

    companion object {

        @Suppress("DEPRECATION")
        fun addResponseToken(authentication: Authentication, response: HttpServletResponse) {
            val claims = HashMap<String, Any?>()
            claims["username"] = authentication.name
            claims["authorities"] = authentication.authorities

            val actualDate = System.currentTimeMillis()
            val expireDate = actualDate + VALIDITY

            //Only for tests purposes
            //val expTimeForTests = 1640995200000L // 2022-01-01 in milliseconds

            val token = Jwts
                    .builder()
                    .setHeaderParam("typ", "JWT")
                    .setClaims(claims)
                    .setSubject(SUBJECT)
                    .setIssuedAt(Date(actualDate))
                    .setExpiration(Date(expireDate))
                    //Only for tests purposes
                    //.setExpiration(Date(expTimeForTests))
                    .signWith(SignatureAlgorithm.HS512, KEY)
                    .compact()

            response.addHeader("Authorization", "Bearer $token")

        }

        fun getClaims(token: String) : Claims = Jwts.parser().setSigningKey(KEY).parseClaimsJws(token).body
    }
}
