package fct.ciai.configurations.security.filters

import fct.ciai.configurations.security.JWTUtil
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.filter.GenericFilterBean
import javax.servlet.FilterChain
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class JWTAuthenticationFilter: GenericFilterBean(){

    override fun doFilter(request: ServletRequest?,
                          response: ServletResponse?,
                          chain: FilterChain?) {
        val authHeader = (request as HttpServletRequest).getHeader("Authorization")

        if(authHeader == null || !authHeader.startsWith("Bearer ")){
            chain!!.doFilter(request, response)
            return
        }

        val token = authHeader.split(" ")[1]
        val claims = JWTUtil.getClaims(token)

        //Should check for token validity here (e.g. expiration date, session in db, etc.)
        val exp = claims["exp"].toString().toLong()
        val currentTime = System.currentTimeMillis() / 1000

        if(exp < currentTime )
            (response as HttpServletResponse).sendError(HttpServletResponse.SC_UNAUTHORIZED) //RFC 6750 3.1
        else
        {
            @Suppress("UNCHECKED_CAST")
            val roles = (claims.get("authorities") as List<String>).map{ SimpleGrantedAuthority(it) }

            val authentication = UserAuthToken(claims["username"] as String, roles)
            //Can go to the db to get user information

            SecurityContextHolder.getContext().authentication = authentication

            //Renew token with extended time here. (before doFilter)
            JWTUtil.addResponseToken(authentication, response as HttpServletResponse)

            chain!!.doFilter(request, response)
        }
    }
}
