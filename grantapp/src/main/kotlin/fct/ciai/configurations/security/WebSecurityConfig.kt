package fct.ciai.configurations.security

import fct.ciai.configurations.security.filters.JWTAuthenticationFilter
import fct.ciai.configurations.security.filters.UserPasswordAuthenticationFilterToJWT
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter
import kotlin.jvm.Throws

@Configuration
@EnableWebSecurity
class WebSecurityConfig : WebSecurityConfigurerAdapter() {
    @Autowired
    lateinit var accountDetails: CustomAccountDetailsService

    @Value("\${server.debug.useJWT}")
    lateinit var useJWT: String

    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {

        if(useJWT == "false") {
            http.csrf().disable()
                    .authorizeRequests()
                    .antMatchers("/cv**").authenticated()
                    .antMatchers("/evaluation_panels**").authenticated()
                    .antMatchers("/grant_applications**").authenticated()
                    .antMatchers("/grant_calls**").permitAll()//authenticated()
                    .antMatchers("/institutions**").authenticated()
                    .antMatchers("/reviews**").authenticated()
                    .antMatchers("/reviewers**").authenticated()
                    .antMatchers("/students**").authenticated()
                    .antMatchers("/swagger-ui.html#/").permitAll()
                    .antMatchers("/swagger-resources/**").permitAll()
                    .antMatchers("/webjars/**").permitAll()
                    .antMatchers("/h2/**").permitAll()
                    .and().httpBasic()
                    .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        }else{
            http.csrf().disable()
                    .authorizeRequests()
                    .antMatchers("/cv**").authenticated()
                    .antMatchers("/evaluation_panels**").authenticated()
                    .antMatchers("/grant_applications**").authenticated()
                    .antMatchers("/grant_calls**").authenticated()
                    .antMatchers("/institutions**").authenticated()
                    .antMatchers("/reviews**").authenticated()
                    .antMatchers("/reviewers**").authenticated()
                    .antMatchers("/students**").authenticated()
                    .antMatchers("/swagger-ui.html#/").permitAll()
                    .antMatchers("/swagger-resources/**").permitAll()
                    .antMatchers("/webjars/**").permitAll()
                    .antMatchers("/h2/**").hasRole("ADMIN")
                    //.and().httpBasic()
                    .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                    .and()
                    .addFilterBefore(
                            UserPasswordAuthenticationFilterToJWT("/login", super.authenticationManagerBean()),
                            BasicAuthenticationFilter::class.java)
                    .addFilterBefore(
                            JWTAuthenticationFilter(),
                            BasicAuthenticationFilter::class.java)
        }

        http.headers().frameOptions().sameOrigin()
    }

    override fun configure(auth: AuthenticationManagerBuilder) {
        auth.inMemoryAuthentication()
                .withUser("admin")
                .password(BCryptPasswordEncoder().encode("admin"))
                .authorities("ROLE_ADMIN")
                //or .roles("ADMIN") is the same
                .and()
                .passwordEncoder(BCryptPasswordEncoder())
                .and()
                .userDetailsService(accountDetails).passwordEncoder(BCryptPasswordEncoder())
    }
}
