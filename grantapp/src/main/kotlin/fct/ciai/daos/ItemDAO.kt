package fct.ciai.daos

import com.sun.istack.NotNull
import javax.persistence.*


@Entity
@Inheritance(strategy = InheritanceType.JOINED)
open class ItemDAO (

        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        open val id: Long,

        @NotNull
        open var type: String,

        @NotNull
        open var header: String,

        @NotNull
        open var subTitle: String
)
