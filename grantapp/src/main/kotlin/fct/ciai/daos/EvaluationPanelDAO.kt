@file:Suppress("JpaDataSourceORMInspection")

package fct.ciai.daos

import com.sun.istack.NotNull
import javax.persistence.*

@Entity
@Table(name = "evaluation_panel")
data class EvaluationPanelDAO(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Long,

        @NotNull
        @ManyToOne
        @JoinColumn(name = "panel_chair_id")
        val panelChair: ReviewerDAO,

        @NotNull
        @ManyToMany
        @JoinTable(name = "evaluation_panel_reviewer",
                joinColumns = [JoinColumn(name = "reviewer_id", referencedColumnName = "id")],
                inverseJoinColumns = [JoinColumn(name = "evaluation_panel_id", referencedColumnName = "id")])
        val panelReviewers: MutableList<ReviewerDAO>

)
