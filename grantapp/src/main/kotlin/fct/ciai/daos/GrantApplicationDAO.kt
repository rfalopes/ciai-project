@file:Suppress("JpaDataSourceORMInspection")

package fct.ciai.daos

import fct.ciai.enums.GrantApplicationStatus
import javax.persistence.*

@Entity
@Table(name = "grant_application")
data class GrantApplicationDAO(

        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Long,

        @Enumerated(EnumType.STRING)
        var status: GrantApplicationStatus,

        @ManyToOne
        @JoinColumn(name = "grant_call_id")
        val grantCall: GrantCallDAO,

        @ManyToOne
        @JoinColumn(name = "student_id")
        val student: StudentDAO,

        @OneToMany(fetch = FetchType.EAGER, mappedBy = "grantApplication", cascade = [CascadeType.REMOVE])
        var fieldsResponses: MutableList<DataItemResponseDAO>,

        @OneToMany(mappedBy = "grantApplication", cascade = [CascadeType.ALL])
        val reviews: MutableList<ReviewDAO>
) {

        constructor(id: Long, call: GrantCallDAO, student: StudentDAO) :
                this(id, GrantApplicationStatus.CREATED, call, student, mutableListOf(), mutableListOf())
}
