package fct.ciai.daos

import lombok.Data
import javax.persistence.*

@Entity
@Data
class DataItemDAO(
        id: Long,
        type: String,
        header: String,
        subTitle: String,
        val mandatory: Boolean,

        @ManyToOne
        @JoinColumn(name = "grant_call_id")
        val grantCall: GrantCallDAO,

        @OneToMany(mappedBy="dataItem", cascade = [CascadeType.ALL])
        val dataItemsResponses : List<DataItemResponseDAO>

) : ItemDAO(id, type, header, subTitle){
        constructor(id: Long,
                    type: String,
                    header: String,
                    subTitle: String,
                    mandatory: Boolean,
                    grantCall: GrantCallDAO) : this(id, type, header, subTitle, mandatory, grantCall, mutableListOf())
}
