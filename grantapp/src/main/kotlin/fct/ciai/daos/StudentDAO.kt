@file:Suppress("EqualsOrHashCode", "JpaDataSourceORMInspection")

package fct.ciai.daos

import fct.ciai.enums.RolesEnum
import lombok.Data
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import java.util.*
import javax.persistence.*

@Entity
@Data
@Table(name = "student")
class StudentDAO(
        id: Long,
        userName: String,
        name: String,
        password: String,
        email: String,
        birthDate: Date,
        institution: InstitutionDAO,

        val average: Double,

        @OneToOne(cascade = [CascadeType.REMOVE])
        val cv: CVDAO,

        @OneToMany(mappedBy = "student", cascade = [CascadeType.ALL])
        val grantApplications: MutableList<GrantApplicationDAO>,

        authorities: MutableList<RolesEnum>


) : UserDAO(id, userName, name, password, email, birthDate, institution, authorities) {

    constructor(
            id: Long,
            userName: String,
            name: String,
            password: String,
            email: String,
            birthDate: Date,
            newCurriculum: CVDAO,
            institution: InstitutionDAO,
            average: Double) :
            this(id, userName, name, BCryptPasswordEncoder().encode(password), email, birthDate, institution,
                    average, newCurriculum, mutableListOf<GrantApplicationDAO>(), mutableListOf(RolesEnum.ROLE_STUDENT))

    constructor(
            id: Long,
            userName: String,
            name: String,
            password: String,
            email: String,
            birthDate: Date,
            institution: InstitutionDAO,
            average: Double) :
            this(
                    id, userName, name, BCryptPasswordEncoder().encode(password), email, birthDate, institution,
                    average, CVDAO(), mutableListOf<GrantApplicationDAO>(), mutableListOf(RolesEnum.ROLE_STUDENT))

    // Only need for test purposes
    override fun equals(other: Any?) : Boolean {
        return (other is StudentDAO)
                && id == other.id
                && name == other.name
                && userName == other.userName
                && email == other.email
                && institution.id == other.institution.id
                && average == other.average
                && cv.id == cv.id
    }
}
