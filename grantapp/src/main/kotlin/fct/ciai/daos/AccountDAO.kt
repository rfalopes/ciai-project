@file:Suppress("EqualsOrHashCode")

package fct.ciai.daos

import com.sun.istack.NotNull
import fct.ciai.enums.RolesEnum
import javax.persistence.*

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
open class AccountDAO(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        open val id: Long,

        @NotNull
        @Column(unique = true)
        open val userName: String,

        @NotNull
        open val name: String,

        @NotNull
        open val password: String,

        @NotNull
        @Column(unique = true)
        open val email: String,

        @ElementCollection
        open var authorities: MutableList<RolesEnum>
) {

    // Only need for test purposes
    override fun equals(other: Any?) = (other is AccountDAO)
            && id == other.id
            && userName == other.userName
            && name == other.name
            && password == other.password
            && email == other.email
}
