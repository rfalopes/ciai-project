package fct.ciai.daos

import lombok.Data
import javax.persistence.*

@Entity
@Data
class DataItemResponseDAO(

        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Long,

        val value: String,

        @ManyToOne
        @JoinColumn(name = "data_item_id")
        val dataItem : DataItemDAO,

        @ManyToOne
        @JoinColumn(name = "grant_application_id")
        var grantApplication : GrantApplicationDAO
)
