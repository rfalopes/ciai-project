@file:Suppress("JpaDataSourceORMInspection", "EqualsOrHashCode")

package fct.ciai.daos

import fct.ciai.enums.RolesEnum
import lombok.Data
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import java.util.*
import javax.persistence.*

@Entity
@Data
@Table(name = "reviewer")
class ReviewerDAO(
        id: Long,
        userName: String,
        name: String,
        password: String,
        email: String,
        birthDate: Date,
        institution: InstitutionDAO,

        @OneToMany(fetch = FetchType.LAZY, mappedBy="reviewer", cascade = [CascadeType.ALL])
        val reviews: MutableList<ReviewDAO>,

        @ManyToMany(mappedBy="panelReviewers")
        val evaluationPanels: MutableList<EvaluationPanelDAO>,

        @OneToMany(mappedBy="panelChair")
        val chairedBy: MutableList<EvaluationPanelDAO>,

        authorities: MutableList<RolesEnum>

) : UserDAO(id, userName, name, password, email, birthDate, institution, authorities) {

        constructor(
                id: Long,
                userName: String,
                name: String,
                password: String,
                email: String,
                birthDate: Date,
                institution: InstitutionDAO) :
                this(
                        id,
                        userName,
                        name,
                        BCryptPasswordEncoder().encode(password),
                        email,
                        birthDate,
                        institution,
                        mutableListOf(),
                        mutableListOf(),
                        mutableListOf(),
                        mutableListOf(RolesEnum.ROLE_REVIEWER)
                )

        override fun equals(other: Any?) : Boolean  {
                return (other is ReviewerDAO)
                        && id == other.id
                        && userName == other.userName
                        && name == other.name
                        && email == other.email
                        && institution == other.institution
                        && reviews.map { it.id } == other.reviews.map { it.id }
                        && evaluationPanels.map { it.id } == other.evaluationPanels.map { it.id }
                        && chairedBy.map { it.id } == other.chairedBy.map { it.id }
        }

}
