@file:Suppress("JpaDataSourceORMInspection")

package fct.ciai.daos

import javax.persistence.*

@Entity
@Table(name = "cv")
data class CVDAO constructor(

        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Long,

        @OneToMany(mappedBy = "cv", cascade = [CascadeType.ALL])
        var cvItems: MutableList<CVItemDAO>

) {
        constructor() : this(0, mutableListOf())
}
