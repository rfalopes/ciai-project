package fct.ciai.enums

enum class GrantCallStatus {
    OPEN, IN_VOTING, CLOSED
}
