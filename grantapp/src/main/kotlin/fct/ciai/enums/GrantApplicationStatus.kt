package fct.ciai.enums

import io.swagger.annotations.ApiModel

@ApiModel
enum class GrantApplicationStatus {
    CREATED, SUBMITTED, IN_VOTING, ACCEPTED, REJECTED
}
