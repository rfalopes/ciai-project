package fct.ciai.enums

import io.swagger.annotations.ApiModel

@ApiModel
enum class ReviewStatus {
    NORMAL, FINAL_ACCEPTED, FINAL_REJECTED
}
