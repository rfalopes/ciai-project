package fct.ciai.authorization.user.reviewers

import org.springframework.security.access.prepost.PreAuthorize
import java.lang.annotation.Inherited


@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForGetAllReviewers.Condition)
annotation class AllowForGetAllReviewers {
    companion object {
        const val Condition = "hasAnyRole({'SPONSOR', 'ADMIN'})"
    }
}

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForGetReviewer.Condition)
annotation class AllowForGetReviewer {
    companion object {
        const val Condition = "hasAnyRole({'ADMIN', 'SPONSOR'}) or " +
                "@reviewersSecurityService.canGetReviewerById(principal,#id)"
    }
}

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForCreateReviewer.Condition)
annotation class AllowForCreateReviewer {
    companion object {
        const val Condition = "hasRole({'ADMIN'})"
    }
}


@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForDeleteReviewer.Condition)
annotation class AllowForDeleteReviewer {
    companion object {
        const val Condition = "hasRole({'ADMIN'})"
    }
}


@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForGetReviewerEvaluationPanels.Condition)
annotation class AllowForGetReviewerEvaluationPanels {
    companion object {
        const val Condition = "hasRole({'ADMIN'}) or " +
        "@reviewersSecurityService.canGetReviewerEvaluationPanels(principal,#id)"

    }
}
