package fct.ciai.authorization.user.reviewers

import fct.ciai.repositories.users.reviewers.ReviewerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.User
import org.springframework.stereotype.Component

@Component("reviewersSecurityService")
class ReviewersSecurityService(
        @Autowired val reviewersRepository: ReviewerRepository
) {
    fun canGetReviewerById(principal: User, reviewerID: Long) : Boolean {
        return userNameMatches(principal, reviewerID)
    }

    fun canGetReviewerEvaluationPanels(principal: User, reviewerID: Long) : Boolean {
        return userNameMatches(principal, reviewerID)
    }

    fun userNameMatches(principal: User, reviewerID: Long) : Boolean{
        val student = reviewersRepository.findById(reviewerID)
        return if(student.isEmpty){
            false
        } else{
            student.get().userName == principal.username
        }
    }


}
