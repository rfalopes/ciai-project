package fct.ciai.authorization.user.students

import org.springframework.security.access.prepost.PreAuthorize
import java.lang.annotation.Inherited

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForCreateStudent.Condition)
annotation class AllowForCreateStudent {
    companion object {
        const val Condition = "hasRole({'ADMIN'})"
    }
}

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER,
        AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForGetStudent.Condition)
annotation class AllowForGetStudent {
    companion object {
        const val Condition = "isAuthenticated() and (hasAnyRole({'ADMIN', 'REVIEWER'}) or " +
                "@studentsSecurityService.canGetStudentById(principal, #userID))"
    }
}

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER,
        AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForDeleteStudent.Condition)
annotation class AllowForDeleteStudent {
    companion object {
        const val Condition = "hasRole({'ADMIN'})"
    }
}

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER,
        AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForGetStudentApplications.Condition)
annotation class AllowForGetStudentApplications {
    companion object {
        const val Condition = "isAuthenticated() and (hasRole({'ADMIN'}) or " +
                "@studentsSecurityService.canGetStudentGrantApplications(principal,#userID))"
    }
}

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER,
        AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForGetAllStudents.Condition)
annotation class AllowForGetAllStudents {
    companion object {
        const val Condition = "hasRole({'ADMIN'})"
    }
}
