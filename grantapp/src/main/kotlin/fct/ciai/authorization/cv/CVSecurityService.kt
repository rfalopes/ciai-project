package fct.ciai.authorization.cv

import fct.ciai.repositories.users.students.StudentRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.User
import org.springframework.stereotype.Component

@Component("cvSecurityService")
class CVSecurityService(
        @Autowired val studentRepository: StudentRepository
) {
    fun isCVOwner(principal: User, cvID: Long): Boolean {

        val student = studentRepository.findByUserName(principal.username)
        if (student.isEmpty) {
            return false
        }
        val studentCVID = student.get().cv.id
        return cvID == studentCVID
    }
}
