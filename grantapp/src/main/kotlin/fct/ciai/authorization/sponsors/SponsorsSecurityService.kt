package fct.ciai.authorization.sponsors

import fct.ciai.repositories.sponsor.SponsorRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.User
import org.springframework.stereotype.Component

@Component("sponsorSecurityService")
class SponsorsSecurityService(
        @Autowired val sponsorRepository: SponsorRepository
) {
    fun canGetSponsorById(principal: User, sponsorID: Long) : Boolean {
        return userNameMatches(principal, sponsorID)
    }

    fun canGetSponsorGrantCalls(principal: User, sponsorID: Long) : Boolean {
        return userNameMatches(principal, sponsorID)
    }

    fun userNameMatches(principal: User, sponsorID: Long) : Boolean{
        val sponsor = sponsorRepository.findById(sponsorID)
        return if(sponsor.isEmpty){
            false
        } else{
            sponsor.get().userName == principal.username
        }
    }


}
