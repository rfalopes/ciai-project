package fct.ciai.authorization.grants.grantApplication

import org.springframework.security.access.prepost.PreAuthorize
import java.lang.annotation.Inherited


@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER,
        AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForGetAllGrantApplications.Condition)
annotation class AllowForGetAllGrantApplications {
    companion object {
        const val Condition = "hasRole({'ADMIN'})"
    }
}

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER,
        AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForGetGrantApplicationById.Condition)
annotation class AllowForGetGrantApplicationById {
    companion object {
        const val Condition = "isAuthenticated() and (" +
                "hasRole({'ADMIN'}) or " +
                "@grantApplicationSecurityService.isGrantCallSponsor(principal, #id) or " +
                "@grantApplicationSecurityService.isGrantApplicationOwner(principal, #id) or " +
                "@grantApplicationSecurityService.isGrantApplicationReviewer(principal, #id)" +
                ")"
    }
}

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER,
        AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForCreateGrantApplication.Condition)
annotation class AllowForCreateGrantApplication {
    companion object {
        const val Condition = "hasAnyRole({'ADMIN', 'STUDENT'})"
    }
}


@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER,
        AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForDeleteGrantApplication.Condition)
annotation class AllowForDeleteGrantApplication {
    companion object {
        const val Condition = "isAuthenticated() and (" +
                "hasRole({'ADMIN'}) or " +
                "@grantCallSecurityService.isGrantApplicationOwner(principal, #id)" +
                ")"
    }
}

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER,
        AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForGetAllGrantApplicationReviews.Condition)
annotation class AllowForGetAllGrantApplicationReviews {
    companion object {
        const val Condition = "isAuthenticated() and (" +
                "hasAnyRole({'ADMIN'}) or " +
                "@grantApplicationSecurityService.isGrantCallSponsor(principal, #id) or " +
                "@grantApplicationSecurityService.isGrantApplicationOwner(principal, #id) or " +
                "@grantApplicationSecurityService.isGrantApplicationReviewer(principal, #id)" +
                ")"
    }
}
