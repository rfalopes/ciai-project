package fct.ciai.authorization.grants.grantCalls

import fct.ciai.repositories.grants.GrantCallRepository
import fct.ciai.repositories.sponsor.SponsorRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.User
import org.springframework.stereotype.Component

@Component("grantCallSecurityService")
class GrantCallSecurityService(
        @Autowired val sponsorRepository: SponsorRepository,
        @Autowired val grantCallRepository: GrantCallRepository
) {
    fun isGrantCallOwner(principal: User, grantCallID: Long): Boolean {
        val sponsor = sponsorRepository.findByUserName(principal.username)
        val grantCalls = grantCallRepository.findById(grantCallID)
        return if (sponsor.isEmpty || grantCalls.isEmpty) {
            false
        } else {
            grantCalls.get().sponsor.id == sponsor.get().id
        }
    }
}
