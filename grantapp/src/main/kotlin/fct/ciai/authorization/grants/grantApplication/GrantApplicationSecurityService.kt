package fct.ciai.authorization.grants.grantApplication

import fct.ciai.repositories.grants.GrantApplicationRepository
import fct.ciai.repositories.users.AccountRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.User
import org.springframework.stereotype.Component

@Component("grantApplicationSecurityService")
class GrantApplicationSecurityService(
        @Autowired val accountRepository: AccountRepository,
        @Autowired val grantApplicationRepository: GrantApplicationRepository
) {
    fun isGrantCallSponsor(principal: User, grantApplicationID: Long): Boolean {
        val account = accountRepository.findByUserName(principal.username)
        val grantApplication = grantApplicationRepository.findById(grantApplicationID)
        return if (account.isEmpty || grantApplication.isEmpty) {
            false
        } else {
            grantApplication.get().grantCall.sponsor.id == account.get().id
        }
    }

    fun isGrantApplicationOwner(principal: User, grantApplicationID: Long): Boolean {
        val account = accountRepository.findByUserName(principal.username)
        val grantApplication = grantApplicationRepository.findById(grantApplicationID)
        return if (account.isEmpty || grantApplication.isEmpty) {
            false
        } else {
            grantApplication.get().student.id == account.get().id
        }
    }

    fun isGrantApplicationReviewer(principal: User, grantApplicationID: Long): Boolean {
        val account = accountRepository.findByUserName(principal.username)
        val grantApplication = grantApplicationRepository.findById(grantApplicationID)
        return if (account.isEmpty || grantApplication.isEmpty) {
            false
        } else {
            grantApplication.get().grantCall.evaluationPanel.panelReviewers.map { it.id }.contains(account.get().id)
        }
    }
}
