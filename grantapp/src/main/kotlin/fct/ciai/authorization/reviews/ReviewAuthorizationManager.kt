package fct.ciai.authorization.reviews

import org.springframework.security.access.prepost.PreAuthorize
import java.lang.annotation.Inherited

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForGetAllReviews.Condition)
annotation class AllowForGetAllReviews {
    companion object {
        const val Condition = "hasRole({'ADMIN'})"
    }
}

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForGetReviewById.Condition)
annotation class AllowForGetReviewById {
    companion object {
        const val Condition = "isAuthenticated() and ("+
                "hasRole({ 'ADMIN'}) or "+
                "@reviewSecurityService.isReviewerInEvaluationPanel(principal,#id) or "+
                "@reviewSecurityService.isAboutTheStudent(principal,#id) or "+
                "@reviewSecurityService.isSponsorOwner(principal,#id))"
    }
}

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForCreateReview.Condition)
annotation class AllowForCreateReview {
    companion object {
        const val Condition = "hasRole({'ADMIN'}) or "+
                "@reviewSecurityService.isCreateAllow(principal,#review.grantApplicationId)"
    }
}

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForCreateFinalReview.Condition)
annotation class AllowForCreateFinalReview {
    companion object {
        const val Condition = "hasRole({'ADMIN'}) or "+
                "@reviewSecurityService.isPanelChairedInEvaluationPanel(principal,#review.grantApplicationId)"
    }
}

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForUpdateReview.Condition)
annotation class AllowForUpdateReview {
    companion object {
        const val Condition ="isAuthenticated() and ("+
                "hasRole({'ADMIN'}) or " +
                "@reviewSecurityService.isOwnerReview(principal,#id))"
    }
}

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER, AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
@PreAuthorize(AllowForDeleteReview.Condition)
annotation class AllowForDeleteReview {
    companion object {
        const val Condition = "isAuthenticated() and (" +
                "hasRole({'ADMIN'}) or " +
                "@reviewSecurityService.isOwnerReview(principal,#id))"

    }
}
