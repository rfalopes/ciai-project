package fct.ciai.authorization.reviews

import fct.ciai.repositories.grants.GrantApplicationRepository
import fct.ciai.repositories.review.ReviewRepository
import fct.ciai.repositories.users.reviewers.ReviewerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.User
import org.springframework.stereotype.Component

@Component("reviewSecurityService")
class ReviewSecurityService(
        @Autowired val reviewRepository: ReviewRepository,
        @Autowired val reviewerRepository: ReviewerRepository,
        @Autowired val grantApplicationRepository: GrantApplicationRepository
)
{

    fun isOwnerReview(principal: User, id : Long) : Boolean{
        return principal.username == reviewRepository.findById(id).get().reviewer.userName
    }

    fun isReviewerInEvaPanel(principal: User, id : Long) : Boolean {
        reviewerRepository.findByUserName(principal.username).get()
        return false
    }

    fun isAboutTheStudent(principal: User, id : Long) : Boolean {
        val review = reviewRepository.findById(id)
        if(review.isEmpty)
            return false

        return review.get().grantApplication.student.userName == principal.username
    }

    fun isReviewerInEvaluationPanel(principal: User, id: Long): Boolean {
        val review = reviewRepository.findById(id)
        if(review.isEmpty)
            return false

        val evaluationPanel = review.get().grantApplication.grantCall.evaluationPanel
        val panelReviewers = evaluationPanel.panelReviewers
        val panelChair = evaluationPanel.panelChair
        return panelReviewers.map{it.userName}.contains(principal.username) || panelChair.userName == principal.username
    }

    fun isSponsorOwner(principal: User, id: Long): Boolean {
        val review = reviewRepository.findById(id)
        if(review.isEmpty)
            return false

        val grantCall = review.get().grantApplication.grantCall
        return grantCall.sponsor.userName == principal.username
    }

    fun isCreateAllow(principal: User, grantApplicationId: Long): Boolean {
        val application = grantApplicationRepository.findById(grantApplicationId)
        return application.get().grantCall.evaluationPanel.panelReviewers.map {it.userName}
                .contains(principal.username)

    }

    fun isPanelChairedInEvaluationPanel(principal: User, grantApplicationId: Long): Boolean {

        val grantApplication = grantApplicationRepository.findById(grantApplicationId)
        if(grantApplication.isEmpty)
            return false
        val panelChair = grantApplication.get().grantCall.evaluationPanel.panelChair
        return panelChair.userName == principal.username
    }


}
