package fct.ciai.services

import fct.ciai.enums.GrantApplicationStatus
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus
import java.lang.RuntimeException

@ResponseStatus(HttpStatus.NOT_FOUND)
class NotFoundException(message: String) : RuntimeException(message)

@ResponseStatus(HttpStatus.CONFLICT)
class ConflictException(message: String) : RuntimeException(message)

@ResponseStatus(HttpStatus.BAD_REQUEST)
class BadRequestException(message: String) : RuntimeException(message)

@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
class MethodNotAllowedException(message: String) : RuntimeException(message)

@ResponseStatus(HttpStatus.FORBIDDEN)
class ForbiddenException(message: String) : RuntimeException(message)

@ResponseStatus(HttpStatus.LOCKED)
class InVotingException : RuntimeException("Grant application is in voting, wait until finish.")

@ResponseStatus(HttpStatus.LOCKED)
class NotSubmitedException : RuntimeException("Grant application is not submited, please submit.")

@ResponseStatus(HttpStatus.LOCKED)
class WaitUntilInVotingTimeException : RuntimeException("Grant application submited, wait until expire date and voting.")

@ResponseStatus(HttpStatus.LOCKED)
class AlreadySubmittedException(status: GrantApplicationStatus) : RuntimeException("Grant application already submited. Actual staatus: $status")

@ResponseStatus(HttpStatus.LOCKED)
class NotClosedGrantCallException : RuntimeException("Not a closed grant call.")
