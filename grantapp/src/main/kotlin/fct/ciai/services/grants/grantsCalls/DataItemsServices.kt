package fct.ciai.services.grants.grantsCalls

import fct.ciai.daos.DataItemDAO
import fct.ciai.repositories.grants.DataItemRepository
import fct.ciai.services.NotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class DataItemsServices(
        @Autowired val dataItemRepository: DataItemRepository
) {

    fun getDataItemById(dataItemID: Long): DataItemDAO {
        val findResult = dataItemRepository.findById(dataItemID)
        if(findResult.isEmpty) {
            throw NotFoundException("Data Item with id $dataItemID does not exit in the system.")
        }
        return findResult.get()
    }
}
