package fct.ciai.services.sponsors

import fct.ciai.daos.SponsorDAO
import fct.ciai.repositories.sponsor.SponsorRepository
import fct.ciai.repositories.users.AccountRepository
import fct.ciai.services.ConflictException
import fct.ciai.services.NotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class SponsorsServices(
        @Autowired val accountRepository: AccountRepository,
        @Autowired val sponsorRepository: SponsorRepository
) {

    fun getAllSponsors(pageable: Pageable): Iterable<SponsorDAO> = sponsorRepository.findAll(pageable)

    fun getAllSponsors(): Iterable<SponsorDAO> = sponsorRepository.findAll()

    fun getSponsorById(id: Long): SponsorDAO {
        val sponsor = sponsorRepository.findById(id)
        if (!sponsor.isPresent)
            throw NotFoundException("Sponsor with id: $id does not exist.")
        return sponsor.get()
    }

    fun addSponsor(sponsorDAO: SponsorDAO): SponsorDAO{
        if(accountRepository.existsByUserNameEqualsOrEmailEquals(sponsorDAO.userName, sponsorDAO.email)){
            throw ConflictException("Sponsor with username ${sponsorDAO.userName} or " +
                    " email ${sponsorDAO.email} already exists in the system.")
        }
        return sponsorRepository.save(sponsorDAO)
    }

    @Transactional
    fun deleteSponsor(id: Long) {
        getSponsorById(id)
        sponsorRepository.deleteById(id)
    }

}
