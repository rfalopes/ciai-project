package fct.ciai.services.reviews

import fct.ciai.daos.ReviewDAO
import fct.ciai.enums.GrantApplicationStatus
import fct.ciai.enums.ReviewStatus
import fct.ciai.repositories.grants.GrantApplicationRepository
import fct.ciai.repositories.review.ReviewRepository
import fct.ciai.services.*
import fct.ciai.services.grants.grantsApplications.GrantApplicationsServices
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class ReviewsServices(
        @Autowired val reviewRepository: ReviewRepository,
        @Autowired val grantApplicationRepository: GrantApplicationRepository,
        @Autowired val grantApplicationsServices: GrantApplicationsServices
) {

    fun getAllReviews(pageable: Pageable): Iterable<ReviewDAO> {
        return reviewRepository.findAll(pageable)
    }

    fun getReviewById(reviewID: Long): ReviewDAO {
        return reviewRepository.findById(reviewID).orElseThrow {
            NotFoundException("Review with id $reviewID not found")
        }
    }

    @Transactional
    fun addReview(reviewDAO: ReviewDAO) {
        val updatedGrantApplication = grantApplicationsServices.verifyAndUpdateStatus(reviewDAO.grantApplication)

        /*Verify if the reviewer can review the grant application*/
        when (updatedGrantApplication.status) {
            GrantApplicationStatus.CREATED -> throw NotSubmitedException()
            GrantApplicationStatus.SUBMITTED -> throw WaitUntilInVotingTimeException()
            else -> {
            } //Continue
        }

        if (reviewRepository.existsByReviewerEqualsAndGrantApplicationEquals(reviewDAO.reviewer, reviewDAO.grantApplication))
            throw ConflictException("This reviewer already made a review about this application")
        /*Create Review*/
        reviewRepository.save(reviewDAO)
    }

    @Transactional
    fun addFinalReview(reviewDAO: ReviewDAO, accepted: Boolean) {


        if (!accepted) {
            val grantApplication = reviewDAO.grantApplication
            grantApplication.status = GrantApplicationStatus.REJECTED
            grantApplicationRepository.save(grantApplication)

            //Add final review
            reviewRepository.save(reviewDAO)
        } else {
            val acceptedGrantApplication = reviewDAO.grantApplication
            val grantApplications = reviewDAO.grantApplication.grantCall.grantApplications
            grantApplications.map {
                if (it.id == acceptedGrantApplication.id)
                    it.status = GrantApplicationStatus.ACCEPTED
                else
                    it.status = GrantApplicationStatus.REJECTED
            }

            //Add final review
            reviewRepository.save(reviewDAO)

            //Update Grant Applications Status
            grantApplicationRepository.saveAll(grantApplications)
        }

    }

    @Transactional
    fun deleteReview(id: Long) {
        getReviewById(id)
        reviewRepository.deleteById(id)
    }

    fun updateReview(id: Long, review: ReviewDAO) {
        val oldReview = getReviewById(id)
        oldReview.update(review)
        reviewRepository.save(oldReview)
    }

    fun checkIfAlreadyReviewed(reviewerID: Long, grantApplicationID: Long) {
        if(reviewRepository.checkIfAlreadyReviewed(reviewerID, grantApplicationID).isEmpty())
            throw ConflictException("Reviewer with id $reviewerID has no review for grant aplication with id $grantApplicationID")
    }
}

