package fct.ciai.services.users.reviewers

import fct.ciai.daos.EvaluationPanelDAO
import fct.ciai.daos.GrantCallDAO
import fct.ciai.daos.ReviewerDAO
import fct.ciai.repositories.evaluationPanels.EvaluationPanelRepository
import fct.ciai.repositories.users.AccountRepository
import fct.ciai.repositories.users.reviewers.ReviewerRepository
import fct.ciai.services.ConflictException
import fct.ciai.services.MethodNotAllowedException
import fct.ciai.services.NotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class ReviewersServices(
        @Autowired val accountRepository: AccountRepository,
        @Autowired val reviewersRepository: ReviewerRepository,
        @Autowired val evaluationPanelRepository: EvaluationPanelRepository
) {

    fun getAllReviewers(pageable: Pageable): Iterable<ReviewerDAO> = reviewersRepository.findAll(pageable)

    fun getAllReviewers(): Iterable<ReviewerDAO> = reviewersRepository.findAll()

    fun getReviewerById(id: Long): ReviewerDAO =
            reviewersRepository.findById(id).orElseThrow {
                NotFoundException("Reviewer with id $id not found")
            }

    fun getReviewersById(ids: List<Long>): MutableList<ReviewerDAO> {
        val list = mutableListOf<ReviewerDAO>()
        for (id in ids)
            list.add(getReviewerById(id))
        return list
    }

    @Transactional
    fun addReviewer(reviewerDAO: ReviewerDAO) {
        if (accountRepository.existsByUserNameEqualsOrEmailEquals(reviewerDAO.userName, reviewerDAO.email)) {
            throw ConflictException("Reviewer with username ${reviewerDAO.userName} or " +
                    " email ${reviewerDAO.email} already exists in the system.")
        }

        /*Create Reviewer*/
        reviewersRepository.save(reviewerDAO)
    }


    fun deleteReviewer(id: Long) {
        val reviewerOptional = reviewersRepository.findById(id)
        if (reviewerOptional.isEmpty)
            throw NotFoundException("Reviewer with id $id does not exit in the system.")

        val reviewerDAO = reviewerOptional.get()

        /*Test if reviewer is a chair*/
        if(reviewerDAO.chairedBy.isNotEmpty()) {
            throw MethodNotAllowedException("Reviewer can not be removed because it is chairing a Grant Call.")
        }

        val evaluationPanelsDAO = reviewerDAO.evaluationPanels

        /*Remove user from all evaluation panels*/
        for (evaluationPanelDAO in evaluationPanelsDAO) {
            evaluationPanelDAO.panelReviewers.remove(reviewerDAO)
            evaluationPanelRepository.save(evaluationPanelDAO)
        }

        /*Delete User*/
        reviewersRepository.deleteById(id)
    }

    fun getAllReviewerEvaluationPanels(reviewerID: Long): List<EvaluationPanelDAO> {
        val reviewer = getReviewerById(reviewerID)
        return reviewer.evaluationPanels
    }

    fun getAllReviewerEvaluationPanelsChairedBy(reviewerID: Long): List<EvaluationPanelDAO> {
        val reviewer = getReviewerById(reviewerID)
        return reviewer.chairedBy
    }

    fun getReviewerByUserName(userName: String): ReviewerDAO {
        return reviewersRepository.findByUserName(userName).orElseThrow {
            NotFoundException("Reviewer with username $userName not found")
        }
    }

    fun getAllGrantCallsToEvaluation(id: Long): List<GrantCallDAO> {
        reviewersRepository.findById(id).orElseThrow {
            NotFoundException("Reviewer with id $id not found.")
        }
        return reviewersRepository.findGrantCallsToEvaluate(id)
    }

    fun getAllChairedGrantCalls(id: Long): List<GrantCallDAO> {
        reviewersRepository.findById(id).orElseThrow {
            NotFoundException("Reviewer with id $id not found.")
        }
        return reviewersRepository.findChairedGrantCalls(id)
    }

    fun checkIfReviewerIsChair(grantCalID: Long, reviewerID: Long) {
        reviewersRepository.findReviewerChairGrantCalls(grantCalID, reviewerID).orElseThrow {
            NotFoundException("Reviewer with id $reviewerID is not chairing Grant Call $grantCalID.")
        }
    }
}
