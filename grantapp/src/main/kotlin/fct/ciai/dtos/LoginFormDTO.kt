package fct.ciai.dtos

class LoginFormDTO (
        val username: String,
        val password: String
){
    constructor() : this("", "")
}
