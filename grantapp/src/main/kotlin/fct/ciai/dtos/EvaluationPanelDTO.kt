package fct.ciai.dtos

import fct.ciai.daos.EvaluationPanelDAO

class EvaluationPanelDTO(
        val id: Long,
        val panelChair: Long,
        val reviewersIDs: MutableList<Long>
) {

    constructor() : this(0, 0, mutableListOf())

    constructor(evaluationPanelDAO: EvaluationPanelDAO) : this(
            evaluationPanelDAO.id,
            evaluationPanelDAO.panelChair.id,
            evaluationPanelDAO.panelReviewers.map { it.id }.toMutableList()
    )

    override fun equals(other: Any?) : Boolean  {
        return (other is EvaluationPanelDTO)
                && id == other.id
                && panelChair == other.panelChair
                && reviewersIDs == other.reviewersIDs
    }
}
