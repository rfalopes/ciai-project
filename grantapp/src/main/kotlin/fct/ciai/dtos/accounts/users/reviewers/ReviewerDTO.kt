package fct.ciai.dtos.accounts.users.reviewers

import fct.ciai.daos.ReviewerDAO
import fct.ciai.dtos.accounts.users.UserDTO
import java.util.*

class ReviewerDTO(id: Long,
                  userName: String,
                  name: String,
                  password: String,
                  email: String,
                  birthDate: Date,
                  institution: Long)
    : UserDTO(id, userName, name, password, email, birthDate, institution)
{
    //Only for tests purposes
    constructor() : this(0,"","","","", Date(),0)

    constructor(reviewer: ReviewerDAO):
            this(
                    reviewer.id,
                    reviewer.userName,
                    reviewer.name,
                    reviewer.password,
                    reviewer.email,
                    reviewer.birthDate,
                    reviewer.institution.id
            )

    //Only for tests purposes
    override fun equals(other: Any?) : Boolean  {
        return (other is ReviewerDTO)
                && id == other.id
                && userName == other.userName
                && name == other.name
                && email == other.email
                && institution == other.institution
    }

}
