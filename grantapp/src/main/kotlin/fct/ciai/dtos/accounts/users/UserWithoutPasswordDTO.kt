package fct.ciai.dtos.accounts.users

import com.fasterxml.jackson.annotation.JsonFormat
import fct.ciai.daos.UserDAO
import fct.ciai.dtos.accounts.AccountWithoutPasswordDTO
import java.util.*

open class UserWithoutPasswordDTO(
        val id: Long,
        val userName: String,
        val name: String,
        val email: String,
        @JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
        val birthDate: Date,
        val institution: Long
) {

    constructor(user: UserDAO) : this(
            user.id,
            user.userName,
            user.name,
            user.email,
            user.birthDate,
            user.institution.id
    )
}
