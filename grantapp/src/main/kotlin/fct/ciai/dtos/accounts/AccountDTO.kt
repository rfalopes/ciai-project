package fct.ciai.dtos.accounts

import fct.ciai.daos.AccountDAO

open class AccountDTO(
        val id: Long,
        val userName: String,
        val name: String,
        val password: String,
        val email: String)
{
    constructor(account: AccountDAO) : this(
            account.id,
            account.userName,
            account.name,
            account.password,
            account.email
    )

}
