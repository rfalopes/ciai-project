package fct.ciai.dtos.accounts

import fct.ciai.daos.AccountDAO

open class AccountWithoutPasswordDTO(
        val id: Long,
        val userName: String,
        val name: String,
        val email: String)
{

    constructor(): this(0, "","","")

    constructor(account: AccountDAO) : this(
            account.id,
            account.userName,
            account.name,
            account.email
    )
}
