package fct.ciai.dtos.grantApplications

import fct.ciai.daos.GrantApplicationDAO
import fct.ciai.dtos.dataitems.DataItemResponseDTO
import fct.ciai.enums.GrantApplicationStatus

open class GrantApplicationDTO(
        val id: Long,
        val grantCallId: Long,
        val studentID: Long,
        val status: GrantApplicationStatus,
        val fieldsResponses: List<DataItemResponseDTO>)
{

    constructor(grantApplicationsDAO: GrantApplicationDAO) : this(
            grantApplicationsDAO.id,
            grantApplicationsDAO.grantCall.id,
            grantApplicationsDAO.student.id,
            grantApplicationsDAO.status,
            grantApplicationsDAO.fieldsResponses.map { DataItemResponseDTO(it) })

    // Only for test purposes
    constructor() : this( 0,0,0, GrantApplicationStatus.IN_VOTING,listOf())

    override fun equals(other: Any?): Boolean {
        return (other is GrantApplicationDTO)
                && this.id == other.id
                && this.grantCallId == other.grantCallId
                && this.studentID == other.studentID
                && this.status == other.status
                && this.fieldsResponses.map {it.id} == other.fieldsResponses.map {it.id}
    }

}


