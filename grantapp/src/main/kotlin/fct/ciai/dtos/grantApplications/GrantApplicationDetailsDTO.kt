package fct.ciai.dtos.grantApplications

import fct.ciai.daos.GrantApplicationDAO
import fct.ciai.dtos.dataitems.DataItemResponseDTO
import fct.ciai.dtos.dataitems.DataItemResponseDetailsDTO
import fct.ciai.enums.GrantApplicationStatus

class GrantApplicationDetailsDTO(
        val id: Long,
        val grantCallId: Long,
        val studentID: Long,
        val status: GrantApplicationStatus,
        val fieldsResponses: List<DataItemResponseDetailsDTO>,
        val grantCallName: String,
        val studentName: String,

) {

    constructor(grantApplicationsDAO: GrantApplicationDAO, grantCallName: String, studentName: String) : this(
            grantApplicationsDAO.id,
            grantApplicationsDAO.grantCall.id,
            grantApplicationsDAO.student.id,
            grantApplicationsDAO.status,
            //REFECTORY: More efficient - Done in phase 4
            grantApplicationsDAO.fieldsResponses.map { DataItemResponseDetailsDTO(it, it.dataItem.header) },
            grantCallName,
            studentName
    )

}
