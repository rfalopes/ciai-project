package fct.ciai.dtos.grantApplications

import fct.ciai.daos.GrantApplicationDAO
import fct.ciai.enums.GrantApplicationStatus

class GrantApplicationForListingDTO(
        val id: Long,
        val grantCallId: Long,
        val studentID: Long,
        val status: GrantApplicationStatus,
        val grantCallName: String
) {
    constructor(grantApplicationsDAO: GrantApplicationDAO, grantCallName: String) : this(
            grantApplicationsDAO.id,
            grantApplicationsDAO.grantCall.id,
            grantApplicationsDAO.student.id,
            grantApplicationsDAO.status,
            grantCallName
    )

}
