package fct.ciai.dtos

import fct.ciai.daos.GrantCallDAO
import fct.ciai.dtos.dataitems.DataItemDTO
import java.util.*

class GrantCallDTO(
        val id: Long,
        val sponsorId: Long,
        val title: String,
        val description: String,
        val startDate: Date,
        val expireDate: Date,
        val dataItems: List<DataItemDTO>,
        val evaluationPanel: EvaluationPanelDTO,
        val funding: Long)
{

    //Only for tests purpose
    constructor() : this(0, 0, "","",Date(),Date(), listOf<DataItemDTO>(), EvaluationPanelDTO(), 0)

    constructor(grantCallDAO: GrantCallDAO) : this(
            grantCallDAO.id,
            grantCallDAO.sponsor.id,
            grantCallDAO.title,
            grantCallDAO.description,
            grantCallDAO.startDate,
            grantCallDAO.expireDate,
            grantCallDAO.dataItems.map { DataItemDTO(it) },
            EvaluationPanelDTO(grantCallDAO.evaluationPanel),
            grantCallDAO.funding,
    )

    //Only for tests purpose
    override fun equals(other: Any?) : Boolean  {
        return (other is GrantCallDTO)
                && id == other.id
                && sponsorId == other.sponsorId
                && title == other.title
                && description == other.description
                && startDate == other.startDate
                && expireDate == other.expireDate
                && dataItems == other.dataItems
                && evaluationPanel == other.evaluationPanel
                && funding == other.funding
    }
}


