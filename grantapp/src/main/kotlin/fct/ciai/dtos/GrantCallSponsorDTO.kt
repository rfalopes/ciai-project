package fct.ciai.dtos

import fct.ciai.daos.GrantCallDAO
import fct.ciai.dtos.dataitems.DataItemDTO
import fct.ciai.enums.GrantCallStatus
import java.util.*

class GrantCallSponsorDTO(
        val id: Long,
        val sponsorId: Long,
        val sponsorName: String,
        val title: String,
        val description: String,
        val startDate: Date,
        val expireDate: Date,
        val dataItems: List<DataItemDTO>,
        val evaluationPanel: EvaluationPanelDTO,
        val funding: Long,
        val numberOfApplications: Int,
        val status: GrantCallStatus
) {

    //Only for tests purpose
    constructor() : this(0, 0, "", "", "", Date(), Date(), listOf<DataItemDTO>(), EvaluationPanelDTO(), 0,0 , GrantCallStatus.CLOSED)

    constructor(grantCallDAO: GrantCallDAO) : this(
            grantCallDAO.id,
            grantCallDAO.sponsor.id,
            grantCallDAO.sponsor.name,
            grantCallDAO.title,
            grantCallDAO.description,
            grantCallDAO.startDate,
            grantCallDAO.expireDate,
            grantCallDAO.dataItems.map { DataItemDTO(it) },
            EvaluationPanelDTO(grantCallDAO.evaluationPanel),
            grantCallDAO.funding,
            grantCallDAO.grantApplications.size,
            grantCallDAO.status,
    )

    //Only for tests purpose
    override fun equals(other: Any?): Boolean {
        return (other is GrantCallSponsorDTO)
                && id == other.id
                && sponsorId == other.sponsorId
                && sponsorName == other.sponsorName
                && title == other.title
                && description == other.description
                && startDate == other.startDate
                && expireDate == other.expireDate
                && dataItems == other.dataItems
                && evaluationPanel == other.evaluationPanel
                && funding == other.funding
    }
}