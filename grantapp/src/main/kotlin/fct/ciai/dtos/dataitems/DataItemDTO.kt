package fct.ciai.dtos.dataitems

import fct.ciai.daos.DataItemDAO

class DataItemDTO constructor (
        id: Long,
        type: String,
        header: String,
        subTitle: String,
        val mandatory: Boolean
): ItemDTO(id, type, header, subTitle){

    constructor() : this(0,"", "", "", false)

    constructor(dataItemDAO : DataItemDAO) : this(
            dataItemDAO.id,
            dataItemDAO.type,
            dataItemDAO.header,
            dataItemDAO.subTitle,
            dataItemDAO.mandatory
    )

    override fun equals(other: Any?)
            = (other is DataItemDTO)
            && id == other.id
            && type == other.type
            && header == other.header
            && subTitle == other.subTitle
            && mandatory == other.mandatory
}
